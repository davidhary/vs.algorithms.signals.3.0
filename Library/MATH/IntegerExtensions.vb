Imports System.Runtime.CompilerServices

''' <summary> Integer value extensions.</summary>
''' <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Module IntegerExtensions

#Region " INTEGER ARRAYS "

    ''' <summary> Returns the index of the first maximum of given array. </summary>
    ''' <param name="values"> The values. </param>
    ''' <returns> The index of the first maximum of given array. </returns>
    <Extension()>
    Public Function IndexFirstMaximum(ByVal values() As Integer) As Integer

        If values Is Nothing OrElse values.Length = 0 Then
            Return -1
        End If

        Dim maxIndex As Integer = values.GetLowerBound(0)
        Dim max As Integer = values(maxIndex)
        For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
            Dim candidate As Integer = values(i)
            If max < candidate Then
                max = candidate
                maxIndex = i
            End If
        Next
        Return maxIndex

    End Function

#End Region

End Module
