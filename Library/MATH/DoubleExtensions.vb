Imports System.Runtime.CompilerServices

''' <summary> Double value extensions. </summary>
''' <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Module DoubleExtensions

#Region " DOUBLE ARRAYS "

    ''' <summary> Adds a value to the Double array elements. </summary>
    ''' <param name="values"> The values. </param>
    ''' <param name="value">  The value. </param>
    <Extension()>
    Public Sub Add(ByVal values() As Double, ByVal value As Double)

        If values IsNot Nothing AndAlso values.Length > 0 Then
            For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                values(i) += value
            Next
        End If

    End Sub

    ''' <summary> Copy values between arrays. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="fromValues"> From values. </param>
    ''' <param name="toValues">   To values. </param>
    <Extension()>
    Public Sub Copy(ByVal fromValues() As Double, ByVal toValues() As Double)

        If fromValues Is Nothing Then
            Throw New ArgumentNullException("fromValues")
        End If
        If toValues Is Nothing Then
            Throw New ArgumentNullException("toValues")
        End If

        If fromValues.Length > 0 AndAlso toValues.Length > 0 Then
            For i As Integer = Math.Max(fromValues.GetLowerBound(0),
                                        toValues.GetLowerBound(0)) To Math.Min(fromValues.GetUpperBound(0),
                                                                               toValues.GetUpperBound(0))
                toValues(i) = fromValues(i)
            Next
        End If

    End Sub

    ''' <summary> Copy values between arrays. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="fromValues"> From values. </param>
    ''' <param name="toValues">   To values. </param>
    <Extension()>
    Public Sub Copy(ByVal fromValues() As Double, ByVal toValues() As Single)

        If fromValues Is Nothing Then
            Throw New ArgumentNullException("fromValues")
        End If
        If toValues Is Nothing Then
            Throw New ArgumentNullException("toValues")
        End If

        If fromValues.Length > 0 AndAlso toValues.Length > 0 Then
            For i As Integer = Math.Max(fromValues.GetLowerBound(0),
                                        toValues.GetLowerBound(0)) To Math.Min(fromValues.GetUpperBound(0),
                                                                               toValues.GetUpperBound(0))
                toValues(i) = CSng(fromValues(i))
            Next
        End If

    End Sub

    ''' <summary> Returns the index of the first maximum of given array. </summary>
    ''' <param name="values"> The values. </param>
    ''' <returns> The index of the first maximum of given array. </returns>
    <Extension()>
    Public Function IndexFirstMaximum(ByVal values() As Double) As Integer

        If values IsNot Nothing AndAlso values.Length > 0 Then
            Dim maxIndex As Integer = values.GetLowerBound(0)
            Dim max As Double = values(maxIndex)
            For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                Dim abs As Double = values(i)
                If max < abs Then
                    max = abs
                    maxIndex = i
                End If
            Next
            Return maxIndex
        Else
            Return -1
        End If

    End Function

    ''' <summary> Calculates the mean of the array elements. </summary>
    ''' <param name="values"> The values. </param>
    ''' <returns> The mean of the array elements. </returns>
    <Extension()>
    Public Function Mean(ByVal values() As Double) As Double

        If values IsNot Nothing AndAlso values.Length > 0 Then
            Return Sum(values) / values.Length
        Else
            Return 0
        End If

    End Function

    ''' <summary> Removes the mean from the values array. </summary>
    ''' <param name="values"> The values. </param>
    <Extension()>
    Public Sub RemoveMean(ByVal values() As Double)

        If values IsNot Nothing AndAlso values.Length > 0 Then
            Add(values, -Mean(values))
        End If

    End Sub

    ''' <summary> Multiplies the array elements by a scalar. </summary>
    ''' <param name="values"> The values. </param>
    ''' <param name="scalar"> The scalar. </param>
    <Extension()>
    Public Sub Scale(ByVal values() As Double, ByVal scalar As Double)

        If values IsNot Nothing AndAlso values.Length > 0 Then
            For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                values(i) *= scalar
            Next
        End If

    End Sub

    ''' <summary> Swaps values between two array locations. </summary>
    ''' <param name="data"> The array containing the data. </param>
    ''' <param name="i">    An array index. </param>
    ''' <param name="j">    An array index. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="i")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="j")>
    <Extension()>
    Public Sub Swap(ByVal data() As Double, ByVal i As Integer, ByVal j As Integer)

        If Not data Is Nothing Then
            Dim cache As Double = data(i)
            data(i) = data(j)
            data(j) = cache
        End If

    End Sub

    ''' <summary> Calculates the sum of the array elements. </summary>
    ''' <param name="values"> The values. </param>
    ''' <returns> The sum of the array elements. </returns>
    <Extension()>
    Public Function Sum(ByVal values() As Double) As Double

        If values IsNot Nothing AndAlso values.Length > 0 Then
            Dim cache As Double = 0
            For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                cache += values(i)
            Next
            Return cache
        Else
            Return 0
        End If

    End Function

    ''' <summary> Calculates the sum of the array elements squared. </summary>
    ''' <param name="values"> The values. </param>
    ''' <returns> The sum of the array elements squared. </returns>
    <Extension()>
    Public Function SumSquares(ByVal values() As Double) As Double

        If values IsNot Nothing AndAlso values.Length > 0 Then
            Dim cache As Double = 0
            Dim value As Double
            For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                value = values(i)
                cache += value * value
            Next
            Return cache
        Else
            Return 0
        End If

    End Function

    ''' <summary> Calculates the root mean square (RMS) value of the array elements. </summary>
    ''' <param name="values"> The values. </param>
    ''' <returns> The root mean square (RMS) value of the array elements. </returns>
    <Extension()>
    Public Function RootMeanSquare(ByVal values() As Double) As Double

        If values IsNot Nothing AndAlso values.Length > 0 Then
            Return Math.Sqrt(SumSquares(values) / values.Length)
        Else
            Return 0
        End If

    End Function

#End Region


End Module
