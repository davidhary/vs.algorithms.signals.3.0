﻿Imports System.Numerics
Imports System.Runtime.CompilerServices

''' <summary> Complex extensions for taper filter. </summary>
Module ComplexTaperFilter

    ''' <summary> Tapers the spectrum using a High Pass taper. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="spectrum">            The spectrum. </param>
    ''' <param name="frequency">           The high pass frequency index. </param>
    ''' <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
    <Extension()>
    Public Sub HighPassTaper(ByVal spectrum As Complex(),
                             ByVal frequency As Integer, ByVal transitionBankCount As Integer)

        If spectrum Is Nothing Then
            Throw New System.ArgumentNullException("spectrum")
        End If

        ComplexTaperFilter.HighPassTaper(spectrum, frequency, transitionBankCount, 0)

    End Sub

    ''' <summary> Tapers the spectrum using a High Pass taper. </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    ''' are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="spectrum">            The spectrum. </param>
    ''' <param name="frequency">           The high pass frequency index. </param>
    ''' <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
    ''' <param name="fromIndex">           The first frequency index form which to apply the high
    ''' pass taper. </param>
    <Extension()>
    Public Sub HighPassTaper(ByVal spectrum As Complex(),
                             ByVal frequency As Integer, ByVal transitionBankCount As Integer, ByVal fromIndex As Integer)

        If spectrum Is Nothing Then
            Throw New System.ArgumentNullException("spectrum")
        End If

        If spectrum.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("spectrum", "The spectrum must have more than a one element.")
        End If

        If fromIndex + transitionBankCount \ 2 > frequency Then
            Throw New ArgumentOutOfRangeException("fromIndex", fromIndex, "Frequency must exceed the minimum frequency plus half the transition band.")
        End If

        Dim elementCount As Integer = spectrum.Length

        If frequency - transitionBankCount \ 2 + transitionBankCount > elementCount \ 2 Then
            Throw New ArgumentOutOfRangeException("frequency", frequency, "Filter frequency plus half transition band must be lower than half the spectrum.")
        End If

        Dim frequencyIndex As Integer
        Dim conjugateFrequencyIndex As Integer
        Dim taperFactor As Double

        ' The Cosine taper scale factor
        Dim deltaPhase As Double
        Dim taperPhase As Double

        If fromIndex = 0 Then
            spectrum(fromIndex) = 0
            fromIndex += 1
        End If

        ' zero out low frequency amplitudes
        frequencyIndex = fromIndex
        conjugateFrequencyIndex = elementCount - frequencyIndex
        Do While frequencyIndex < (frequency - transitionBankCount \ 2)

            spectrum(frequencyIndex) = 0
            spectrum(conjugateFrequencyIndex) = 0

            frequencyIndex += 1
            conjugateFrequencyIndex -= 1

        Loop

        ' Set the cosine values  
        taperPhase = 0
        deltaPhase = 0.5 * Math.PI / Convert.ToSingle(transitionBankCount)

        ' taper through the frequency transition band from the data
        Do While frequencyIndex < frequency - transitionBankCount \ 2 + transitionBankCount

            taperFactor = 0.5 * (1.0 + Math.Cos(taperPhase))

            ' Apply the taper factor      
            spectrum(frequencyIndex) = taperFactor * spectrum(frequencyIndex)
            spectrum(conjugateFrequencyIndex) = taperFactor * spectrum(conjugateFrequencyIndex)

            '  Set the next point
            taperPhase += deltaPhase
            frequencyIndex += 1
            conjugateFrequencyIndex -= 1

        Loop

    End Sub

    ''' <summary> Applies a Low Pass Filter in the frequency domain by tapering the signal spectrum. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="spectrum">            The spectrum. </param>
    ''' <param name="frequency">           The low pass frequency index. </param>
    ''' <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
    <Extension()>
    Public Sub LowPassTaper(ByVal spectrum As Complex(),
                            ByVal frequency As Integer, ByVal transitionBankCount As Integer)

        If spectrum Is Nothing Then
            Throw New System.ArgumentNullException("spectrum")
        End If

        ComplexTaperFilter.LowPassTaper(spectrum, frequency, transitionBankCount, spectrum.Length \ 2)

    End Sub

    ''' <summary> Applies a Low Pass Filter in the frequency domain by tapering the signal spectrum. </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    ''' are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="spectrum">            The spectrum. </param>
    ''' <param name="frequency">           The low pass frequency index. </param>
    ''' <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
    ''' <param name="toIndex">             Zero-based index of to. </param>
    <Extension()>
    Public Sub LowPassTaper(ByVal spectrum As Complex(),
                            ByVal frequency As Integer, ByVal transitionBankCount As Integer, ByVal toIndex As Integer)

        If spectrum Is Nothing Then
            Throw New System.ArgumentNullException("spectrum")
        End If

        If spectrum.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("spectrum", "The spectrum must have more than a one element.")
        End If

        If frequency - transitionBankCount \ 2 <= 0 Then
            Throw New ArgumentOutOfRangeException("frequency", frequency, "Filter frequency minus half the transition band must exceed zero.")
        End If

        If frequency - transitionBankCount \ 2 + transitionBankCount > toIndex Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Maximum frequency must exceed the filter frequency plus half the transition band.")
        End If

        Dim elementCount As Integer = spectrum.Length

        If toIndex > (elementCount \ 2) Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Maximum frequency must be lower than half the spectrum.")
        End If

        Dim frequencyIndex As Integer
        Dim conjugateFrequencyIndex As Integer
        Dim taperFactor As Double

        ' The Cosine taper scale factor
        Dim deltaPhase As Double
        Dim taperPhase As Double

        ' Set the cosine values  
        taperPhase = 0
        deltaPhase = 0.5 * Math.PI / Convert.ToSingle(transitionBankCount)

        ' taper through the frequency transition band from the data
        frequencyIndex = frequency - transitionBankCount \ 2
        conjugateFrequencyIndex = elementCount - frequencyIndex
        Do While frequencyIndex < frequency - transitionBankCount \ 2 + transitionBankCount

            taperFactor = 0.5 * (1.0 + Math.Cos(taperPhase))

            ' Apply the taper factor      
            spectrum(frequencyIndex) = taperFactor * spectrum(frequencyIndex)
            spectrum(conjugateFrequencyIndex) = taperFactor * spectrum(conjugateFrequencyIndex)

            '  Set the next point
            taperPhase += deltaPhase
            frequencyIndex += 1
            conjugateFrequencyIndex -= 1

        Loop

        Do While frequencyIndex <= toIndex

            spectrum(frequencyIndex) = 0
            spectrum(conjugateFrequencyIndex) = 0

            frequencyIndex += 1
            conjugateFrequencyIndex -= 1

        Loop

    End Sub

End Module
