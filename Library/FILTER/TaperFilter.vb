Imports System.Numerics

''' <summary> Provides shared services for applying taper filter windows in the frequency domain. </summary>
''' <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public NotInheritable Class TaperFilter

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Initializes a new instance of the <see cref="TaperFilter" /> class. </summary>
    ''' <param name="filterType"> Type of the filter. </param>
    Public Sub New(ByVal filterType As TaperFilterType)
        MyBase.new()
        Me.FilterType = filterType
    End Sub

#End Region

#Region " SHARED "

    ''' <summary> Validates the filter frequencies. </summary>
    ''' <param name="filterType">     Type of the filter. </param>
    ''' <param name="samplingRate">   The sampling rate. </param>
    ''' <param name="lowFrequency">   The low frequency. </param>
    ''' <param name="highFrequency">  The high frequency. </param>
    ''' <param name="transitionBand"> The transition band. </param>
    ''' <param name="details">        [in,out] The outcome of the validation. </param>
    ''' <returns> <c>True</c> if passed; otherwise, <c>False</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="5#")>
    Public Shared Function ValidateFilterFrequencies(ByVal filterType As isr.Algorithms.Signals.TaperFilterType,
                                                     ByVal samplingRate As Double,
                                                     ByVal lowFrequency As Double, ByVal highFrequency As Double,
                                                     ByVal transitionBand As Double,
                                                     ByRef details As String) As Boolean

        Dim validated As Boolean = True

        If samplingRate <= 0 Then
            details = String.Format(Globalization.CultureInfo.CurrentCulture, "Sampling rate ({0}) must be positive.", samplingRate)
            validated = False
        ElseIf transitionBand <= 0 Then
            details = String.Format(Globalization.CultureInfo.CurrentCulture, "Transition band ({0}) must be positive.", transitionBand)
            validated = False
        ElseIf filterType = TaperFilterType.HighPass Then
            If highFrequency - transitionBand / 2 <= 0 Then
                validated = False
                details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "High Pass Filter frequency ({0}) must exceed half the transition band ({1}).",
                                        highFrequency, transitionBand / 2)
            ElseIf highFrequency + transitionBand / 2 > samplingRate / 2 Then
                validated = False
                details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "High Pass Filter frequency ({0}) plus half transition band ({1}) must be lower than half the sampling rate ({2}).",
                                        highFrequency, transitionBand / 2, samplingRate / 2)
            End If
        ElseIf filterType = TaperFilterType.LowPass Then
            If lowFrequency - transitionBand / 2 <= 0 Then
                validated = False
                details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Low Pass Filter frequency ({0}) must exceed half the transition band ({1}).",
                                        lowFrequency, transitionBand / 2)
            ElseIf lowFrequency + transitionBand / 2 > samplingRate / 2 Then
                validated = False
                details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Low Pass Filter frequency ({0}) plus half transition band ({1}) must be lower than half the sampling rate ({2}).",
                                        highFrequency, transitionBand / 2, samplingRate / 2)
            End If
        ElseIf filterType = TaperFilterType.BandPass Then
            If lowFrequency - transitionBand / 2 <= 0 Then
                validated = False
                details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Low pass-band filter frequency ({0}) must exceed half the transition band ({1}).",
                                        lowFrequency, transitionBand / 2)
            ElseIf highFrequency - transitionBand < lowFrequency Then
                validated = False
                details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "High pass-band filter frequency ({0}) must exceed the low frequency ({1}) plus the transition band ({2}).",
                                        highFrequency, lowFrequency, transitionBand)
            ElseIf highFrequency + transitionBand / 2 > samplingRate / 2 Then
                validated = False
                details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "High pass-band frequency ({0}) plus half transition band ({1}) must be lower than half the sampling rate ({2}).",
                                        highFrequency, transitionBand / 2, samplingRate / 2)
            End If
        ElseIf filterType = TaperFilterType.BandReject Then
            If lowFrequency - transitionBand / 2 <= 0 Then
                validated = False
                details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Low stop-band filter frequency ({0}) must exceed half the transition band ({1}).",
                                        lowFrequency, transitionBand / 2)
            ElseIf highFrequency - transitionBand < lowFrequency Then
                validated = False
                details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "High stop-band filter frequency ({0}) must exceed the low frequency ({1}) plus the transition band ({2}).",
                                        highFrequency, lowFrequency, transitionBand)
            ElseIf highFrequency + transitionBand / 2 > samplingRate / 2 Then
                validated = False
                details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "High stop-band frequency ({0}) plus half transition band ({1}) must be lower than half the sampling rate ({2}).",
                                        highFrequency, transitionBand / 2, samplingRate / 2)
            End If
        End If

        Return validated

    End Function

    ''' <summary> Returns the index in the spectrum matching the given frequency. </summary>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="frequency">        The frequency. </param>
    ''' <param name="samplingRate">     The sampling rate. </param>
    ''' <param name="timeSeriesLength"> Length of the time series. </param>
    ''' <returns> The index in the spectrum matching the given frequency. </returns>
    Private Shared Function FrequencyIndex(ByVal frequency As Double, ByVal samplingRate As Double, ByVal timeSeriesLength As Integer) As Integer

        If samplingRate <= Single.Epsilon Then
            Throw New ArgumentOutOfRangeException("samplingRate", "Sampling rate must be positive")
        End If

        If timeSeriesLength <= 2 Then
            Throw New ArgumentOutOfRangeException("timeSeriesLength", "Time series length must exceed 2")
        End If

        If frequency <= Single.Epsilon Then
            Throw New ArgumentOutOfRangeException("frequency", "Frequency must be positive")
        End If

        Return CInt(frequency * timeSeriesLength / samplingRate)

    End Function

#End Region

#Region " METHODS "

    ''' <summary> Validates filter settings. </summary>
    ''' <param name="details"> [in,out] The validation failure details. </param>
    ''' <returns> <c>True</c> if validated; otherwise, <c>False</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="0#")>
    Public Function ValidateFilterFrequencies(ByRef details As String) As Boolean
        Return TaperFilter.ValidateFilterFrequencies(Me.FilterType, Me.SamplingRate, Me.LowFrequency,
                                                     Me.HighFrequency, Me.TransitionBand, details)
    End Function

    ''' <summary> Tapers the frequency band for filtering the signal in the frequency domain. </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    ''' are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    ''' invalid. </exception>
    ''' <param name="spectrum">     The spectrum. </param>
    ''' <param name="samplingRate"> The sampling rate. </param>
    Public Sub Taper(ByVal spectrum As Complex(), ByVal samplingRate As Double)

        If spectrum Is Nothing Then
            Throw New ArgumentNullException("spectrum")
        End If

        If spectrum.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("spectrum", "The spectrum must have more than a one element.")
        End If

        Me.SamplingRate = samplingRate
        Me.TimeSeriesLength = spectrum.Length

        Dim details As String = ""
        If Not Me.ValidateFilterFrequencies(details) Then
            Throw New InvalidOperationException(details)
        End If

        Select Case Me.FilterType

            Case TaperFilterType.BandPass

                ' high pass at the low frequency
                ComplexTaperFilter.HighPassTaper(spectrum, Me.FrequencyIndex(Me.LowFrequency), Me.FrequencyIndex(Me.TransitionBand))

                ' low pass at the high frequency.
                ComplexTaperFilter.LowPassTaper(spectrum, Me.FrequencyIndex(Me.HighFrequency), Me.FrequencyIndex(Me.TransitionBand))

            Case TaperFilterType.BandReject

                Dim midFrequency As Double = (Me.LowFrequency + Me.HighFrequency) / 2

                ' low pass at the low frequency but up to the mid band
                ComplexTaperFilter.LowPassTaper(spectrum,
                                                Me.FrequencyIndex(Me.LowFrequency), Me.FrequencyIndex(Me.TransitionBand),
                                                Me.FrequencyIndex(midFrequency))

                ' high pass at the high frequency but only from the mid band
                ComplexTaperFilter.HighPassTaper(spectrum, Me.FrequencyIndex(Me.HighFrequency),
                                          Me.FrequencyIndex(Me.TransitionBand), Me.FrequencyIndex(midFrequency))

            Case TaperFilterType.HighPass

                ComplexTaperFilter.HighPassTaper(spectrum, Me.FrequencyIndex(Me.HighFrequency), Me.FrequencyIndex(Me.TransitionBand))

            Case TaperFilterType.LowPass

                ComplexTaperFilter.LowPassTaper(spectrum, Me.FrequencyIndex(Me.LowFrequency), Me.FrequencyIndex(Me.TransitionBand))

            Case TaperFilterType.None

        End Select

    End Sub

    ''' <summary> Returns the frequency index for the specified frequency. </summary>
    ''' <param name="frequency"> The frequency. </param>
    ''' <returns> The frequency index for the specified frequency. </returns>
    Private Function frequencyIndex(ByVal frequency As Double) As Integer
        Return TaperFilter.FrequencyIndex(frequency, Me.SamplingRate, Me.TimeSeriesLength)
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets or sets the <see cref="isr.Algorithms.Signals.TaperFilterType">type</see>
    ''' of filter. </summary>
    ''' <value> The type of the filter. </value>
    Public Property FilterType() As isr.Algorithms.Signals.TaperFilterType

    ''' <summary> Gets or sets the low frequency of the filter. </summary>
    ''' <value> The low frequency. </value>
    Public Property LowFrequency() As Double

    ''' <summary> Gets or sets the high frequency of the filter. </summary>
    ''' <value> The high frequency. </value>
    Public Property HighFrequency() As Double

    ''' <summary> Gets or sets the filter transition band. </summary>
    ''' <value> The transition band. </value>
    Public Property TransitionBand() As Double

    ''' <summary> Gets or sets the sampling rate for calculating frequency indexes. </summary>
    ''' <value> The sampling rate. </value>
    Private Property SamplingRate() As Double

    ''' <summary> Gets or sets the number of elements in the time series sample that is processed to
    ''' compute the Spectrum. </summary>
    ''' <value> The length of the time series. </value>
    Public Property TimeSeriesLength() As Integer

#End Region

End Class
