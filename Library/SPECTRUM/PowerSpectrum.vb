Imports System.Numerics

''' <summary> The base class for the power spectrum. </summary>
''' <license> (c) 1998 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class PowerSpectrum
    Inherits Spectrum

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Constructs This class. </summary>
    ''' <param name="fourierTransform"> Holds reference to the Fourier Transform class to use for
    ''' calculating the spectrum. </param>
    Public Sub New(ByVal fourierTransform As isr.Algorithms.Signals.FourierTransformBase)

        ' instantiate the base class
        MyBase.New(fourierTransform)

    End Sub

#End Region

#Region " WELCH METHOD "

    ''' <summary> Appends the spectrum to the existing spectrum. </summary>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    ''' invalid. </exception>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    ''' are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="spectrum"> The spectrum. </param>
    Public Sub Append(ByVal spectrum() As Complex)

        If Me.Scaled Then
            Throw New InvalidOperationException("Because densities were already scaled they must be restored before adding move averages to the power spectrum.")
        End If
        If spectrum Is Nothing Then
            Throw New System.ArgumentNullException("spectrum")
        End If
        If spectrum.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("spectrum", "Spectrum must be longer than 1")
        End If

        If Me.Densities Is Nothing Then
            ReDim _densities(spectrum.Length - 1)
        End If
        For i As Integer = 0 To Me.Densities.Length - 1
            Dim abs As Double = spectrum(i).Abs
            Me.Densities(i) += abs * abs
        Next

        ' increment the average count
        Me._averageCount += 1

    End Sub

    ''' <summary> Number of averages. </summary>
    Private _averageCount As Integer

    ''' <summary> Gets or sets the average count. </summary>
    ''' <value> The average count. </value>
    Public ReadOnly Property AverageCount() As Integer
        Get
            Return Me._averageCount
        End Get
    End Property

#End Region

#Region " DENSITIES AND MAGNITUDES "

    ''' <summary> Clears and allocates the power spectrum densities. </summary>
    ''' <param name="length"> Specifies the length of the spectrum thus allowing for a Int16er spectrum
    ''' than is calculated. Specify 0 to let the program set the maximum length. </param>
    Public Sub Clear(ByVal length As Integer)

        Me.Scaled = False
        Me._averageCount = 0
        If length > 0 Then
            ReDim _densities(length - 1)
        Else
            Me._densities = Nothing
        End If

    End Sub

    ''' <summary> Copies the density and the number of averages. </summary>
    ''' <param name="values">        The values. </param>
    ''' <param name="averageCount">  Specifies the number of averages used to calculate the densities. </param>
    ''' <param name="spectrumCount"> Specifies the number of points to copy. </param>
    Public Sub CopyToDensities(ByVal values() As Double, ByVal averageCount As Integer, ByVal spectrumCount As Integer)

        If values Is Nothing Then
            Me._densities = Nothing
        Else
            Me.Scaled = True
            Me._averageCount = averageCount
            ReDim _densities(spectrumCount - 1)
            Array.Copy(values, Me._densities, spectrumCount)
        End If

    End Sub

    ''' <summary> Copies the density and the number of averages. </summary>
    ''' <param name="values">        The values. </param>
    ''' <param name="averageCount">  Specifies the number of averages used to calculate the densities. </param>
    ''' <param name="spectrumCount"> Specifies the number of points to copy. </param>
    Public Sub CopyToDensities(ByVal values() As Single, ByVal averageCount As Integer, ByVal spectrumCount As Integer)

        If values Is Nothing Then
            Me._densities = Nothing
        Else
            Me.Scaled = True
            Me._averageCount = averageCount
            ReDim _densities(spectrumCount - 1)
            Array.Copy(values, Me._densities, spectrumCount)
        End If

    End Sub

    ''' <summary> Returns the indexed density. </summary>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    ''' invalid because spectrum densities were not created. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="halfSpectrum"> Specifies is the scaling is doubled. </param>
    ''' <param name="index">        The index. </param>
    ''' <returns> The indexed density. </returns>
    Public Function Density(ByVal halfSpectrum As Boolean, ByVal index As Integer) As Double
        If Me.Densities Is Nothing Then
            Throw New InvalidOperationException("Spectrum densities where not created.")
        End If
        If index < Me.Densities.GetLowerBound(0) Then
            Throw New ArgumentOutOfRangeException("index", index, "Index must exceed lower bound")
        End If
        If index > Me.Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("index", index, "Index must not exceed spectrum length")
        End If

        If Me.Scaled Then

            If halfSpectrum Then

                If index = 0 Then
                    Return Me.Densities(index)
                Else
                    Dim tempValue As Double = Me.Densities(index)
                    Return tempValue + tempValue
                End If

            Else

                Return Me.Densities(index)

            End If

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(halfSpectrum)
            If index = 0 And halfSpectrum Then
                Return 0.5R * factor * Me.Densities(index)
            Else
                Return factor * Me.Densities(index)
            End If

        End If

    End Function

    ''' <summary> The densities. </summary>
    Private _densities() As Double

    ''' <summary> Returns the un-scaled (if not scaled) spectrum densities. </summary>
    ''' <remarks> Unlike SRE5, the spectrum densities are scaled for full spectrum rather than as a
    ''' half spectrum.  This simplifies greatly the retrieval of spectrum magnitude or power for
    ''' display. </remarks>
    ''' <returns> The un-scaled (if not scaled) spectrum densities. </returns>
    Public Overloads Function Densities() As Double()
        Return Me._densities
    End Function

    ''' <summary> Returns the scaled spectrum densities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid
    ''' because spectrum densities were not created. </exception>
    ''' <returns> The scaled densities. </returns>
    Public Overloads Function ScaledDensities() As Double()

        If Me.Densities Is Nothing Then
            Throw New InvalidOperationException("Spectrum densities where not created.")
        End If

        ' allocate the outcome array
        Dim output(Me.Densities.Length - 1) As Double

        If Me.Scaled Then

            ReDim output(Me.Densities.Length - 1)
            For i As Integer = 0 To Me.Densities.Length - 1
                output(i) = Me.Densities(i)
            Next

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(False)
            For i As Integer = 0 To Me.Densities.Length - 1
                output(i) = factor * Me.Densities(i)
            Next

        End If

        Return output

    End Function

    ''' <summary> Returns the scaled spectrum densities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid
    ''' because spectrum densities were not created. </exception>
    ''' <param name="halfSpectrum"> Specifies is the scaling is doubled. </param>
    ''' <returns> The scaled spectrum densities. </returns>
    Public Overloads Function ScaledDensities(ByVal halfSpectrum As Boolean) As Double()

        If Me.Densities Is Nothing Then
            Throw New InvalidOperationException("Spectrum densities where not created.")
        End If

        Dim output() As Double

        If Me.Scaled Then

            If halfSpectrum Then

                ReDim output(MyBase.HalfSpectrumLength - 1)
                output(0) = Me.Densities(0)
                For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                    Dim tempValue As Double = Me.Densities(i)
                    output(i) = tempValue + tempValue
                Next

            Else

                ReDim output(Me.Densities.Length - 1)
                For i As Integer = 0 To Me.Densities.Length - 1
                    output(i) = Me.Densities(i)
                Next

            End If

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(False)

            If halfSpectrum Then

                ReDim output(MyBase.HalfSpectrumLength - 1)
                output(0) = 0.5R * factor * Me.Densities(0)
                For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                    Dim tempValue As Double = factor * Me.Densities(i)
                    output(i) = tempValue + tempValue
                Next

            Else

                ReDim output(Me.Densities.Length - 1)
                For i As Integer = 0 To Me.Densities.Length - 1
                    output(i) = factor * Me.Densities(i)
                Next

            End If

        End If

        Return output

    End Function

    ''' <summary> Returns the scaled spectrum densities. </summary>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    ''' invalid because spectrum densities were not created. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="halfSpectrum"> Specifies is the scaling is doubled for displaying the half
    ''' spectrum. </param>
    ''' <param name="fromIndex">    From index. </param>
    ''' <param name="toIndex">      To index. </param>
    ''' <returns> The scaled spectrum densities. </returns>
    Public Overloads Function ScaledDensities(ByVal halfSpectrum As Boolean, ByVal fromIndex As Integer, ByVal toIndex As Integer) As Double()

        If Me.Densities Is Nothing Then
            Throw New InvalidOperationException("Spectrum densities where not created.")
        End If
        If fromIndex < Me.Densities.GetLowerBound(0) Then
            Throw New ArgumentOutOfRangeException("fromIndex", fromIndex, "Starting index must exceed lower bound")
        End If
        If fromIndex > Me.Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("fromIndex", fromIndex, "Starting index must not exceed spectrum length")
        End If
        If toIndex < fromIndex Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Ending index must exceed starting index")
        End If
        If toIndex > Me.Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Ending index must not exceed spectrum length")
        End If

        ' allocate outcome space
        Dim output(toIndex - fromIndex) As Double

        If Me.Scaled Then

            If halfSpectrum Then

                Dim j As Integer = 0
                If fromIndex = 0 Then
                    output(j) = Me.Densities(fromIndex)
                    fromIndex += 1
                    j += 1
                End If
                For i As Integer = fromIndex To toIndex
                    Dim tempValue As Double = Me.Densities(i)
                    output(j) = tempValue + tempValue
                    j += 1
                Next

            Else

                Dim j As Integer = 0
                ReDim output(toIndex - fromIndex)
                For i As Integer = fromIndex To toIndex
                    output(j) = Me.Densities(i)
                    j += 1
                Next

            End If

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(halfSpectrum)

            Dim j As Integer = 0
            If fromIndex = 0 And halfSpectrum Then
                output(j) = 0.5R * factor * Me.Densities(fromIndex)
                fromIndex += 1
                j += 1
            End If

            ' Scale the rest of the spectrum
            For i As Integer = fromIndex To toIndex
                output(j) = factor * Me.Densities(i)
                j += 1
            Next i

        End If

        Return output

    End Function

    ''' <summary> Returns the length of the densities array. </summary>
    ''' <value> The length of the densities. </value>
    Public ReadOnly Property DensitiesLength() As Integer
        Get
            If Me.Densities Is Nothing Then
                Return 0
            Else
                Return Me.Densities.Length
            End If
        End Get
    End Property

    ''' <summary> Returns the indexed magnitude. </summary>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    ''' invalid because spectrum densities were not created. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="halfSpectrum"> Specifies is the scaling is doubled. </param>
    ''' <param name="index">        The index. </param>
    ''' <returns> The indexed magnitude. </returns>
    Public Function Magnitude(ByVal halfSpectrum As Boolean, ByVal index As Integer) As Double

        If Me.Densities Is Nothing Then
            Throw New InvalidOperationException("Spectrum densities where not created.")
        End If
        If index < Me.Densities.GetLowerBound(0) Then
            Throw New ArgumentOutOfRangeException("index", index, "Index must exceed lower bound")
        End If
        If index > Me.Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("index", index, "Index must not exceed spectrum length")
        End If

        Return Math.Sqrt(Me.Density(halfSpectrum, index))

    End Function

    ''' <summary> Returns the scaled magnitude spectrum. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid
    ''' because spectrum densities were not created. </exception>
    ''' <returns> The scaled magnitude spectrum. </returns>
    Public Function ScaledMagnitudes() As Double()
        If Me.Densities Is Nothing Then
            Throw New InvalidOperationException("Spectrum densities where not created.")
        End If

        ' allocate the outcome array
        Dim output(Me.Densities.Length - 1) As Double

        If Me.Scaled Then

            ReDim output(Me.Densities.Length - 1)
            For i As Integer = 0 To Me.Densities.Length - 1
                output(i) = Math.Sqrt(Me.Densities(i))
            Next

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(False)
            For i As Integer = 0 To Me.Densities.Length - 1
                output(i) = Math.Sqrt(factor * Me.Densities(i))
            Next

        End If

        Return output

    End Function

    ''' <summary> Returns the scaled magnitude spectrum. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid
    ''' because spectrum densities were not created. </exception>
    ''' <param name="halfSpectrum"> Specifies is the scaling is doubled. </param>
    ''' <returns> The scaled magnitude spectrum. </returns>
    Public Function ScaledMagnitudes(ByVal halfSpectrum As Boolean) As Double()
        If Me.Densities Is Nothing Then
            Throw New InvalidOperationException("Spectrum densities where not created.")
        End If

        Dim output() As Double

        If Me.Scaled Then

            If halfSpectrum Then

                ReDim output(MyBase.HalfSpectrumLength - 1)
                output(0) = Math.Sqrt(Me.Densities(0))
                For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                    Dim tempValue As Double = Me.Densities(i)
                    output(i) = Math.Sqrt(tempValue + tempValue)
                Next

            Else

                ReDim output(Me.Densities.Length - 1)
                For i As Integer = 0 To Me.Densities.Length - 1
                    output(i) = Math.Sqrt(Me.Densities(i))
                Next

            End If

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(False)

            If halfSpectrum Then

                ReDim output(MyBase.HalfSpectrumLength - 1)
                output(0) = Math.Sqrt(0.5R * factor * Me.Densities(0))
                For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                    Dim tempValue As Double = factor * Me.Densities(i)
                    output(i) = Math.Sqrt(tempValue + tempValue)
                Next

            Else

                ReDim output(Me.Densities.Length - 1)
                For i As Integer = 0 To Me.Densities.Length - 1
                    output(i) = Math.Sqrt(factor * Me.Densities(i))
                Next

            End If

        End If

        Return output

    End Function

    ''' <summary> Returns the scaled magnitude spectrum. </summary>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    ''' invalid because spectrum densities were not created. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="halfSpectrum"> Specifies is the scaling is doubled for displaying the half
    ''' spectrum. </param>
    ''' <param name="fromIndex">    From index. </param>
    ''' <param name="toIndex">      To index. </param>
    ''' <returns> The scaled magnitude spectrum. </returns>
    Public Function ScaledMagnitudes(ByVal halfSpectrum As Boolean, ByVal fromIndex As Integer, ByVal toIndex As Integer) As Double()

        If Me.Densities Is Nothing Then
            Throw New InvalidOperationException("Spectrum densities where not created.")
        End If
        If fromIndex < Me.Densities.GetLowerBound(0) Then
            Throw New ArgumentOutOfRangeException("fromIndex", fromIndex, "Starting index must exceed lower bound")
        End If
        If fromIndex > Me.Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("fromIndex", fromIndex, "Starting index must not exceed spectrum length")
        End If
        If toIndex < fromIndex Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Ending index must exceed starting index")
        End If
        If toIndex > Me.Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Ending index must not exceed spectrum length")
        End If

        ' allocate outcome space
        Dim output(toIndex - fromIndex) As Double

        If Me.Scaled Then

            If halfSpectrum Then

                Dim j As Integer = 0
                If fromIndex = 0 Then
                    output(j) = Math.Sqrt(Me.Densities(fromIndex))
                    fromIndex += 1
                    j += 1
                End If
                For i As Integer = fromIndex To toIndex
                    Dim tempValue As Double = Me.Densities(i)
                    output(j) = Math.Sqrt(tempValue + tempValue)
                    j += 1
                Next

            Else

                Dim j As Integer = 0
                ReDim output(toIndex - fromIndex)
                For i As Integer = fromIndex To toIndex
                    output(j) = Math.Sqrt(Me.Densities(i))
                    j += 1
                Next

            End If

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(halfSpectrum)

            Dim j As Integer = 0
            If fromIndex = 0 And halfSpectrum Then
                output(j) = Math.Sqrt(0.5R * factor * Me.Densities(fromIndex))
                fromIndex += 1
                j += 1
            End If

            ' Scale the rest of the spectrum
            For i As Integer = fromIndex To toIndex
                output(j) = Math.Sqrt(factor * Me.Densities(i))
                j += 1
            Next i

        End If

        Return output

    End Function

    ''' <summary> Returns the index of the spectrum peak. </summary>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    ''' invalid because spectrum densities were not created. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="fromIndex"> From index. </param>
    ''' <param name="toIndex">   To index. </param>
    ''' <returns> The index of the spectrum peak. </returns>
    Public Function PeakIndex(ByVal fromIndex As Integer, ByVal toIndex As Integer) As Integer

        If Me.Densities Is Nothing Then
            Throw New InvalidOperationException("Spectrum densities where not created.")
        End If
        If fromIndex < Me.Densities.GetLowerBound(0) Then
            Throw New ArgumentOutOfRangeException("fromIndex", fromIndex, "Starting index must exceed lower bound")
        End If
        If fromIndex > Me.Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("fromIndex", fromIndex, "Starting index must not exceed spectrum length")
        End If
        If toIndex < fromIndex Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Ending index must exceed starting index")
        End If
        If toIndex > Me.Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Ending index must not exceed spectrum length")
        End If

        ' Set Max to first value
        Dim maxDensityIndex As Integer = fromIndex
        Dim maxDensity As Single = CSng(Me.Densities(maxDensityIndex))
        Dim maxCandidate As Single
        For i As Integer = fromIndex To Math.Min(toIndex, Me.HalfSpectrumLength - 1)

            ' Get candidate
            maxCandidate = CSng(Me.Densities(i))

            ' Select a new maximum if lower than element.
            If maxCandidate > maxDensity Then
                maxDensity = maxCandidate
                maxDensityIndex = i
            End If

        Next i

        ' return the index
        Return maxDensityIndex

    End Function

#End Region

#Region " POWER "

    ''' <summary> Returns the total power for the specified index range but using both side of the
    ''' spectrum and assuming the spectrum is for real valued time series, that is, that the spectrum
    ''' is symmetric. </summary>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    ''' invalid because spectrum densities were not created. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="fromIndex"> From index. </param>
    ''' <param name="toIndex">   To index. </param>
    ''' <returns> The total power for the specified index range but using both side of the spectrum and
    ''' assuming the spectrum is for real valued time series, that is, that the spectrum is
    ''' symmetric. </returns>
    Public Function TotalPower(ByVal fromIndex As Integer, ByVal toIndex As Integer) As Double
        If Me.Densities Is Nothing Then
            Throw New InvalidOperationException("Spectrum densities where not created.")
        End If
        If fromIndex < Me.Densities.GetLowerBound(0) Then
            Throw New ArgumentOutOfRangeException("fromIndex", fromIndex, "Starting index must exceed lower bound")
        End If
        If fromIndex > Me.Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("fromIndex", fromIndex, "Starting index must not exceed spectrum length")
        End If
        If toIndex < fromIndex Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Ending index must exceed starting index")
        End If
        If toIndex > Me.Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Ending index must not exceed spectrum length")
        End If

        ' the DC spectrum is scaled by half because the Spectrum Scale Factor
        ' is derived for the half spectrum (twice as large).
        Dim total As Double = 0
        If fromIndex = 0 Then
            total += 0.5 * Me.Densities(0)
            fromIndex += 1
        End If
        ' go up to half spectrum
        For i As Integer = fromIndex To Math.Min(toIndex, Me.HalfSpectrumLength - 1)
            ' add the power
            total += Me.Densities(i)
        Next

        If Not Me.Scaled Then
            ' apply double the spectrum power factor to correct for the two-sided spectrum.
            total *= Me.SpectrumScaleFactor(True)
        Else
            ' apply double the spectrum power factor to correct for the two-sided spectrum.
            total *= 2
        End If

        Return total

    End Function

    ''' <summary> Returns the total power for the specified index range. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid
    ''' because spectrum densities were not created. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="fromIndex"> From index. </param>
    ''' <returns> The total power for the specified index range. </returns>
    Public Function TotalPower(ByVal fromIndex As Integer) As Double
        If Me.Densities Is Nothing Then
            Throw New InvalidOperationException("Spectrum densities where not created.")
        End If
        Return TotalPower(fromIndex, Me.Densities.GetUpperBound(0))
    End Function

    ''' <summary> Returns the total power. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid
    ''' because spectrum densities were not created. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <returns> The total power. </returns>
    Public Function TotalPower() As Double
        If Me.Densities Is Nothing Then
            Throw New InvalidOperationException("Spectrum densities where not created.")
        End If
        Return TotalPower(Me.Densities.GetLowerBound(0), Me.Densities.GetUpperBound(0))
    End Function

#End Region

#Region " SCALE "

    ''' <summary> Restore the densities to their pre-scaled values so that additional averages can be
    ''' added. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    Public Sub Restore()

        Dim factor As Double = Me.SpectrumScaleFactor(False)
        If factor <= Single.Epsilon Then
            Throw New InvalidOperationException("Invalid scale factor not caught by the Spectrum Scale Factor function--Contact Developer.")
        Else
            factor = 1.0R / factor
        End If

        ' un-scale the rest of the spectrum
        For i As Integer = 0 To Me.Densities.GetUpperBound(0)
            Me.Densities(i) *= factor
        Next i

        ' un-tag power spectrum as not scaled allowing adding spectrum averages.
        Me.Scaled = False

    End Sub

    ''' <summary> Scales the spectrum densities based on the number of averages. </summary>
    Public Sub Scale()

        Dim factor As Double = Me.SpectrumScaleFactor(False)

        ' Scale the rest of the spectrum
        For i As Integer = 0 To Me.Densities.GetUpperBound(0)
            Me.Densities(i) *= factor
        Next i

        ' tag power spectrum as scaled.  This prevents adding new averages until
        ' the spectrum is cleared or restored.
        Me.Scaled = True

    End Sub

    ''' <summary> Returns the spectrum scale factor including the effect of the taper window power. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="halfSpectrum"> Specifies is the scaling is doubled for displaying the half
    ''' spectrum. </param>
    ''' <returns> The spectrum scale factor including the effect of the taper window power. </returns>
    Public Function SpectrumScaleFactor(ByVal halfSpectrum As Boolean) As Double
        If MyBase.TimeSeriesLength = 0 Then
            Throw New InvalidOperationException("Invalid scale factor--The spectrum time series is set to zero.")
        ElseIf Me.AverageCount = 0 Then
            Throw New InvalidOperationException("Invalid scale factor--Average count is zero.")
        End If
        Dim factor As Double = 1 / (Convert.ToDouble(Me.AverageCount) * MyBase.TimeSeriesLength * MyBase.TimeSeriesLength)
        If halfSpectrum Then
            ' The factor of two ensure double the power for half spectrum.
            factor *= 2
        End If
        ' add taper window power.
        If Not MyBase.TaperWindow Is Nothing Then
            If MyBase.TaperWindow.Power <= Single.Epsilon Then
                Throw New InvalidOperationException("Invalid scale factor--Taper window has zero power.")
            End If
            factor /= MyBase.TaperWindow.Power
        End If
        Return factor
    End Function

    ''' <summary> Gets or sets the condition for the densities were scaled.  Once the densities are
    ''' scaled no more averages can be appended until the spectrum is
    ''' <see cref="Restore">restored</see> </summary>
    ''' <value> The scaled sentinel. </value>
    Private Property Scaled As Boolean

#End Region

#Region " TRANSFORM "

    ''' <summary> Calculates the spectrum for complex data and appends to the existing spectrum. </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    ''' are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="timeSeries"> The time series. </param>
    Public Sub TransformAppend(ByVal timeSeries() As Complex)

        If timeSeries Is Nothing Then
            Throw New System.ArgumentNullException("timeSeries")
        End If
        If timeSeries.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("timeSeries", "Time series must be longer than 1")
        End If

        ' calculate the spectrum
        MyBase.Calculate(timeSeries)

        ' add the spectrum
        Me.Append(timeSeries)

    End Sub

    ''' <summary> Calculates the spectrum for real data and appends to the existing spectrum. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="timeSeries"> The time series. </param>
    ''' <param name="dataPoints"> Specifies the number of data points to use. </param>
    Public Sub TransformAppend(ByVal timeSeries() As Complex, ByVal dataPoints As Integer)

        If timeSeries Is Nothing Then
            Throw New System.ArgumentNullException("timeSeries")
        End If

        If dataPoints = timeSeries.Length Then

            TransformAppend(timeSeries)

        Else

            ' Get the time series data
            Dim ts(dataPoints - 1) As Complex
            Array.Copy(timeSeries, ts, Math.Min(ts.Length, timeSeries.Length))
            TransformAppend(ts)

        End If

    End Sub

    ''' <summary> Calculates the spectrum for real data and appends to the existing spectrum. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="timeSeries"> The time series. </param>
    ''' <param name="dataPoints"> Specifies the number of data points to use. </param>
    Public Sub TransformAppend(ByVal timeSeries() As Double, ByVal dataPoints As Integer)

        If timeSeries Is Nothing Then
            Throw New System.ArgumentNullException("timeSeries")
        End If

        Dim ts(dataPoints - 1) As Complex
        timeSeries.CopyTo(ts)
        TransformAppend(ts)

    End Sub

#End Region

#Region " FILE I/O "

    ''' <summary> Reads spectrum densities from the . </summary>
    ''' <remarks> Returns values as an array. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="reader"> Specifies reference to an open binary reader. </param>
    Public Overloads Sub ReadDensities(ByVal reader As IO.BinaryReader)

        If reader Is Nothing Then
            Throw New ArgumentNullException("reader")
        End If

        ' assume the densities were scaled.
        Me.Scaled = True

        ' read the densities parameters
        Me._averageCount = reader.ReadInt32
        Dim elementCount As Integer = reader.ReadInt32

        ' allocate data array
        ReDim _densities(elementCount - 1)

        ' Read the densities from the file
        For i As Integer = 0 To elementCount - 1
            Me.Densities(i) = reader.ReadDouble
        Next i

    End Sub

    ''' <summary> Writes the densities to a data file.  The densities are written as scaled. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    ''' null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid
    ''' because spectrum densities were not created. </exception>
    ''' <param name="writer"> Specifies reference to an open binary writer. </param>
    Public Overloads Sub WriteDensities(ByVal writer As System.IO.BinaryWriter)

        If writer Is Nothing Then
            Throw New ArgumentNullException("writer")
        End If

        If Me.Densities Is Nothing Then
            Throw New InvalidOperationException("Spectrum densities where not created.")
        End If

        ' write the spectrum parameters
        writer.Write(Me.AverageCount)

        Dim elementCount As Integer = Me.Densities.Length
        writer.Write(elementCount)

        If Me.Scaled Then
            ' write values to the file
            For i As Integer = 0 To elementCount - 1
                writer.Write(Me.Densities(i))
            Next i
        Else
            Dim factor As Double = Me.SpectrumScaleFactor(False)
            For i As Integer = 0 To elementCount - 1
                writer.Write(factor * Me.Densities(i))
            Next i
        End If

    End Sub

#End Region

End Class

