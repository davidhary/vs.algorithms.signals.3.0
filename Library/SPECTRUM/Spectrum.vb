Imports System.Numerics

''' <summary> Calculates the spectrum using the Fourier Transform.  Includes pre-processing fro
''' removing mean and applying a taper data window. Includes post-processing methods for high-
''' pass and low-pass filtering in the frequency domain. </summary>
''' <license> (c) 1998 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class Spectrum
    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Constructs This class. </summary>
    ''' <param name="fourierTransform"> Holds reference to the Fourier Transform class to use for
    ''' calculating the spectrum. </param>
    Public Sub New(ByVal fourierTransform As isr.Algorithms.Signals.FourierTransformBase)

        ' instantiate the base class
        MyBase.New()

        ' set reference to the Fourier Transform class
        Me._fourierTransform = fourierTransform

    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks> Do not make This method Overridable (virtual) because a derived class should not be
    ''' able to override This method. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> Gets (private) the dispose status sentinel. </summary>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed() As Boolean

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks> Executes in two distinct scenarios as determined by its disposing parameter.  If True,
    ''' the method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed. </remarks>
    ''' <param name="disposing"> True if This method releases both managed and unmanaged resources;
    ''' False if This method releases only unmanaged resources. </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                    ' remove the reference to the external objects.

                End If

                ' Free shared unmanaged resources
                Me._fourierTransform = Nothing
                Me._TaperWindow = Nothing

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> The Fourier transform. </summary>
    Private _fourierTransform As isr.Algorithms.Signals.FourierTransformBase

    ''' <summary> Gets reference to the Fourier Transform used for calculating the spectrum. </summary>
    ''' <value> The Fourier transform. </value>
    Protected ReadOnly Property FourierTransform() As isr.Algorithms.Signals.FourierTransformBase
        Get
            Return Me._fourierTransform
        End Get
    End Property

    ''' <summary> Length of the half spectrum. </summary>
    Private _HalfSpectrumLength As Integer

    ''' <summary> The number of elements in half the spectrum including the DC point. </summary>
    ''' <value> The length of the half spectrum. </value>
    Public ReadOnly Property HalfSpectrumLength() As Integer
        Get
            Return Me._HalfSpectrumLength
        End Get
    End Property

    ''' <summary> Gets or Sets the condition as True scale the Spectrum.  It is useful to set This to
    ''' false whenever averaging transforms so that the scaling is done at the end. </summary>
    ''' <value> The is scale FFT. </value>
    Public Property IsScaleFft() As Boolean

    ''' <summary> Gets or Sets the condition as True remove the mean before computing the Spectrum. </summary>
    ''' <value> The is remove mean. </value>
    Public Property IsRemoveMean() As Boolean

    ''' <summary> Returns the power (average sum of squares of amplitudes) of the taper window. </summary>
    ''' <remarks> Represents the average amount by which the power of the signal was attenuated due to
    ''' the tapering effect of the taper data window. </remarks>
    ''' <value> The taper window power. </value>
    Public ReadOnly Property TaperWindowPower() As Double
        Get
            If Me.TaperWindow Is Nothing Then
                Return 1.0R
            Else
                Return Me.TaperWindow.Power()
            End If
        End Get
    End Property

    ''' <summary> Length of the time series. </summary>
    Private _TimeSeriesLength As Integer

    ''' <summary> Gets or sets the number of elements in the time series sample that is processed to
    ''' compute the Spectrum. </summary>
    ''' <remarks> This must be set before setting the filter frequencies. The size of the Fourier
    ''' transform is set when calling the Fourier Transform. That call is used to update the time
    ''' series length. This value is allowed to be set independently in case the spectrum is read
    ''' from a file. </remarks>
    ''' <value> The length of the time series. </value>
    Public Property TimeSeriesLength() As Integer
        Get
            Return Me._TimeSeriesLength
        End Get
        Set(value As Integer)
            Me._TimeSeriesLength = value
            Me._HalfSpectrumLength = CInt(Math.Floor(Me.TimeSeriesLength / 2) + 1)
        End Set
    End Property

    ''' <summary> Gets or sets the low pass frequency.  Must be set before setting the filter
    ''' frequencies. </summary>
    ''' <value> The sampling rate. </value>
    Public Property SamplingRate() As Double

    ''' <summary> Gets or sets reference to the <see cref="isr.Algorithms.Signals.TaperFilter">taper
    ''' Filter</see>. </summary>
    ''' <value> The taper filter. </value>
    Public Property TaperFilter() As isr.Algorithms.Signals.TaperFilter

    ''' <summary> Gets or sets reference to the <see cref="isr.Algorithms.Signals.TaperWindow">taper
    ''' window</see>
    ''' used for reducing side lobes. </summary>
    ''' <value> The taper window. </value>
    Public Property TaperWindow() As isr.Algorithms.Signals.TaperWindow

#End Region

#Region " CALCULATE "

    ''' <summary> Applies the filter described by values. </summary>
    ''' <param name="values"> The input time series where the spectrum is returned. </param>
    Public Sub ApplyFilter(ByVal values As Complex())
        If Me.TaperFilter IsNot Nothing AndAlso Me.TaperFilter.FilterType <> TaperFilterType.None Then
            Me.TaperFilter.Taper(values, Me.SamplingRate)
        End If
    End Sub

    ''' <summary> Applies the taper window. </summary>
    ''' <param name="timeSeries"> The time series. </param>
    Public Sub ApplyTaperWindow(ByVal timeSeries As Complex())
        If Me.TaperWindow IsNot Nothing Then
            Me.TaperWindow.Apply(timeSeries)
        End If
    End Sub

    ''' <summary> Removes the mean. </summary>
    ''' <param name="timeSeries"> The time series. </param>
    Public Sub RemoveMean(ByVal timeSeries As Complex())
        If Me.IsRemoveMean Then
            timeSeries.RemoveRealMean()
        End If
    End Sub

    ''' <summary> Returns the FFT scale factor taking into account the effect of the taper window. </summary>
    ''' <param name="timeSeriesLength"> Length of the time series. </param>
    ''' <returns> The FFT scale factor taking into account the effect of the taper window. </returns>
    Public Function FftScaleFactor(ByVal timeSeriesLength As Integer) As Double
        Return 1.0 / (System.Math.Sqrt(Me.TaperWindowPower) * timeSeriesLength)
    End Function

    ''' <summary> Scales the specified spectrum if <see cref="IsScaleFft">option</see> is set. </summary>
    ''' <param name="spectrum"> The spectrum. </param>
    Public Sub ScaleFft(ByVal spectrum As Complex())
        ' ' Scale the Spectrum if the scale option is set
        If spectrum IsNot Nothing AndAlso Me.IsScaleFft Then
            spectrum.Scale(Me.FftScaleFactor(spectrum.Length))
        End If
    End Sub

    ''' <summary> Calculates the spectrum using the Fourier Transform with pre- and post-processing. </summary>
    ''' <remarks> Applies filter, removes mean, calculates the spectrum and appends to the previous
    ''' spectrum. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    ''' are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="values"> The input time series where the spectrum is returned. </param>
    Public Sub Calculate(ByVal values As Complex())

        If values Is Nothing Then
            Throw New System.ArgumentNullException("values")
        End If

        If values.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("values", "Time series must have more than 1 value")
        End If

        ' Remove the mean if the mean option is set
        Me.RemoveMean(values)

        ' Apply the taper window
        Me.ApplyTaperWindow(values)

        ' Calculate the forward Fourier transform  
        Me.FourierTransform.Forward(values)

        ' scale if required
        Me.ScaleFft(values)

        ' apply the filter as necessary
        Me.ApplyFilter(values)

    End Sub

    ''' <summary> Filters the real valued signal using low pass and high pass taper frequency windows. </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    ''' are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="timeSeries"> The time series. </param>
    Public Sub Filter(ByVal timeSeries() As Complex)

        If timeSeries Is Nothing Then
            Throw New System.ArgumentNullException("timeSeries")
        End If

        If timeSeries.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("timeSeries", "Time series must be longer than 1")
        End If

        ' Calculate the forward Fourier transform  
        Me.FourierTransform.Forward(timeSeries)

        ' Scale the Spectrum.
        timeSeries.Scale(1.0R / timeSeries.Length)

        ' apply the filter as necessary
        If Me.TaperFilter IsNot Nothing AndAlso Me.TaperFilter.FilterType <> TaperFilterType.None Then
            Me.ApplyFilter(timeSeries)
        End If

        ' inverse transform
        Me.FourierTransform.Inverse(timeSeries)

    End Sub

    ''' <summary> Initializes the spectrum. </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    ''' are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="timeSeries"> The time series. </param>
    Public Sub Initialize(ByVal timeSeries() As Complex)

        If timeSeries Is Nothing Then
            Throw New System.ArgumentNullException("timeSeries")
        End If
        If timeSeries.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("timeSeries", "Time series must be longer than 1")
        End If

        ' Create the taper window
        If Me.TaperWindow IsNot Nothing Then
            Me.TaperWindow.Create(timeSeries.Length)
        End If

        ' initialize the transform.
        Me.FourierTransform.Initialize(timeSeries)
        Me.TimeSeriesLength = Me.FourierTransform.TimeSeriesLength

    End Sub

#End Region

End Class

