Imports System.Numerics

''' <summary>The base class for the applying taper data windows to signals before 
'''   calculating the Fourier transform.</summary>
''' <license>
''' (c) 1998 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <remarks>
''' See http://en.wikipedia.org/wiki/Window_function
''' </remarks>
Public MustInherit Class TaperWindow

    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="TaperWindow" /> class.
    ''' </summary>
    ''' <param name="windowType">Type of the window.</param>
    Protected Sub New(ByVal windowType As TaperWindowType)
        MyBase.New()
        Me._windowType = windowType
    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make This method Overridable (virtual) because a derived 
    '''   class should not be able to override This method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>Gets or sets (private) the dispose status sentinel.</summary>
    Protected Property IsDisposed() As Boolean

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if This method releases both managed and unmanaged 
    '''   resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me._amplitudes = Nothing

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " PROPERTIES "

    Private _amplitudes() As Double

    ''' <summary> Returns the taper window amplitudes. </summary>
    ''' <returns> The amplitudes of the taper window. </returns>
    Public Function Amplitudes() As Double()
        Return Me._amplitudes
    End Function

    ''' <summary> Calculates the taper window amplitude for the provided element. The element number is
    ''' between 0 to N-1. </summary>
    ''' <param name="element"> The element. </param>
    ''' <returns> The amplitude for the provided element. </returns>
    Public MustOverride Function Amplitude(ByVal element As Integer) As Double

    ''' <summary> Calculates the taper window amplitudes. </summary>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="elementCount"> Holds the element count of the time series. </param>
    ''' <returns> The taper window power. </returns>
    Public Function Create(ByVal elementCount As Integer) As Double

        If elementCount < 2 Then
            Throw New ArgumentOutOfRangeException("elementCount", elementCount, "Must be greater than 1")
        End If

        ' check if window already created.
        If Me.ElementCount = elementCount Then
            Return Me.Power
        End If

        ' set the size.
        Me.ElementCount = elementCount

        ' allocate the amplitude time series
        ReDim _amplitudes(elementCount - 1)

        ' holds the amplitude of the window
        Dim taperFactor As Double   ' The window amplitude

        Dim first As Integer = Me.Amplitudes.GetLowerBound(0)
        Dim last As Integer = Me.Amplitudes.GetUpperBound(0)

        ' initialize the sum of squares
        Dim ssq As Double = 0.0R

        ' apply the same taper factor amplitude to the first and last indexes.
        Do While first < last

            ' calculate the window factor
            taperFactor = Amplitude(first)

            ' Get the sum of powers
            ssq += taperFactor * taperFactor

            ' Save the amplitude
            Me._amplitudes(first) = taperFactor
            Me._amplitudes(last) = taperFactor

            ' Shift indexes
            first += 1
            last -= 1

        Loop

        ' add the two sides
        ssq += ssq

        ' if odd value we need one more calculation
        If (elementCount Mod 2) = 1 Then
            ' calculate the window factor
            taperFactor = Amplitude(first)
            ssq += taperFactor * taperFactor
            Me._amplitudes(first) = taperFactor
        End If

        ' Get the average power
        Me._power = ssq / Convert.ToDouble(elementCount)

        Return Me.Power

    End Function

    ''' <summary> Applies the taper window. </summary>
    ''' <param name="timeSeries"> Holds the time series amplitude where the modified signal is
    ''' returned. </param>
    ''' <returns> The taper window power. </returns>
    Public Function Apply(ByVal timeSeries() As Complex) As Double

        If timeSeries IsNot Nothing AndAlso timeSeries.Length > 0 Then
            If Me.ElementCount <> timeSeries.Length Then

                ' if new length, recalculate the window.
                Create(timeSeries.Length)

            End If

            ' apply the window
            timeSeries.ScaleReals(Me.Amplitudes)

            ' return the power 
            Return Me.Power
        Else
            Return 0
        End If


    End Function

    ''' <summary>Gets or sets the base phase.  This value equals 2*Pi/N for cosine-based
    '''   Windows and 2/N for linear (e.g., Parzen or Bartlett) Windows.</summary>
    Protected Property BasePhase() As Double

    Private _timeSeriesLength As Integer
    ''' <summary>Returns the number of elements in the time series sample
    '''   that was used to compute the taper window.
    ''' </summary>
    Public ReadOnly Property TimeSeriesLength() As Integer
        Get
            Return Me._timeSeriesLength
        End Get
    End Property

    Private _elementCount As Integer
    ''' <summary>Gets or sets the local size of the taper window.</summary>
    Protected Overridable Property ElementCount() As Integer
        Get
            Return Me._elementCount
        End Get
        Set(ByVal Value As Integer)
            Me._elementCount = Value
            Me._timeSeriesLength = Value
        End Set
    End Property

    Private _power As Double
    ''' <summary>Gets or sets the power (average sum of squares of amplitudes) of the 
    '''   taper window.</summary>
    ''' <remarks>Represents the average amount by which the power of the signal was 
    '''   attenuated due to the tapering effect of the taper data window.
    ''' </remarks>
    Public ReadOnly Property Power() As Double
        Get
            Return Me._power
        End Get
    End Property

    Private _windowType As TaperWindowType
    ''' <summary>Gets or sets the window type.</summary>
    Public ReadOnly Property WindowType() As TaperWindowType
        Get
            Return Me._windowType
        End Get
    End Property

#End Region

End Class

''' <summary>Applies a Bartlett taper window.</summary>
''' <remarks>The Bartlett window implements a absolute linear window:
'''   A =  1 - | 2 * k / (N-1) |, k=0,1,...N-1.
''' </remarks>
''' <license>
''' (c) 1998 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class BartlettTaperWindow
    Inherits TaperWindow

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs This class.</summary>
    Public Sub New()

        ' instantiate the base class
        MyBase.New(TaperWindowType.Bartlett)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if This method releases both managed and unmanaged 
    '''   resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary>Gets or sets the length of the window last created. Used 
    '''   by inherited classes to set the base phase.</summary>
    Protected Overrides Property ElementCount() As Integer
        Get
            Return MyBase.ElementCount
        End Get
        Set(ByVal Value As Integer)
            MyBase.ElementCount = Value
            MyBase.BasePhase = 2 / Convert.ToDouble(Value - 1)
        End Set
    End Property

    ''' <summary>Calculates the Bartlett taper window amplitude for the provided element.</summary>
    ''' <param name="element">The element number (i) for calculating the phase.</param>
    ''' <returns> The amplitude for the provided element. </returns>
    Public Overloads Overrides Function Amplitude(ByVal element As Integer) As Double

        Return 1 - Math.Abs(MyBase.BasePhase * element)

    End Function

#End Region

End Class

''' <summary>Applies a Blackman-Harris taper window.</summary>
''' <remarks>The Blackman window implements a third order cosine window aimed at reducing side lobes.</remarks>
''' <license>
''' (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class BlackmanHarrisTaperWindow
    Inherits CosineTaperWindow

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs This class.</summary>
    Public Sub New()

        ' instantiate the base class
        MyBase.New(TaperWindowType.BlackmanHarris)
        Me.A0 = 0.355768
        Me.A1 = 0.487396
        Me.A2 = 0.144232
        Me.A3 = 0.012604
    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if This method releases both managed and unmanaged 
    '''   resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    ' onDisposeManagedResources

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

End Class


''' <summary>Applies a Blackman taper window.</summary>
''' <remarks>The Blackman window implements a second order cosine window:
'''   A =  0.42 - 0.5 * cos2a + 0.08 * cos4a
'''   where a is the base angle Pi*k/N, k=0,1,...N-1.
''' </remarks>
''' <license>
''' (c) 1998 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class BlackmanTaperWindow
    Inherits TaperWindow

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs This class.</summary>
    Public Sub New()

        ' instantiate the base class
        MyBase.New(TaperWindowType.Blackman)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if This method releases both managed and unmanaged 
    '''   resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    ' onDisposeManagedResources

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " METHODS AND PROPERTIES "

    ' Exact Blackman Window coefficients
    Private Const a0 As Double = 7938 / 18608
    Private Const a1 As Double = 9240 / 18608
    Private Const a2 As Double = 1430 / 18608
    Private Const a3 As Double = 0.0R

    ''' <summary>Gets or sets the length of the window last created. Used 
    '''   by inherited classes to set the base phase.</summary>
    Protected Overrides Property ElementCount() As Integer
        Get
            Return MyBase.ElementCount
        End Get
        Set(ByVal Value As Integer)
            MyBase.ElementCount = Value
            MyBase.BasePhase = 2 * Math.PI / Convert.ToDouble(Value - 1)
        End Set
    End Property

    ''' <summary> Calculates the Blackman taper window amplitude for the provided element. The phase is
    ''' calculated as 2 * PI * i /(N - 1), i = 0 to N-1. </summary>
    ''' <param name="element"> The element number (i) for calculating the phase. </param>
    ''' <returns> The amplitude for the provided element. </returns>
    Public Overloads Overrides Function Amplitude(ByVal element As Integer) As Double

        Dim cos2a As Double = Math.Cos(MyBase.BasePhase * element)
        Dim cos4a As Double = 2.0R * cos2a * cos2a - 1.0R
        Return a0 - a1 * cos2a + a2 * cos4a

    End Function

#End Region

End Class

''' <summary>Applies a Cosine taper window.</summary>
''' <remarks>The Cosine window implements a generalized Window for the following format:
'''   A =  A0 - A1 * cos2a + A2 * cos4a - A3 * cos6a
'''   where a is the base angle Pi*k/N, k=0,1,...N-1.
'''   The default values of the cosine window are set for the Blackman window, 
'''   i.e., A0 = 0.42, A1 = 0.5, A2 = 0.08, and A3 = 0.
''' </remarks>
''' <license>
''' (c) 1998 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class CosineTaperWindow
    Inherits TaperWindow

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="TaperWindow" /> class.
    ''' </summary>
    ''' <param name="windowType">Type of the window.</param>
    Protected Sub New(ByVal windowType As TaperWindowType)
        MyBase.New(windowType)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="CosineTaperWindow" /> class.
    ''' </summary>
    ''' <remarks>
    ''' Initializes the coefficients to using the Exact Blackman Window.
    ''' </remarks>
    Public Sub New()

        ' instantiate the base class
        MyBase.New(TaperWindowType.Cosine)
        Me._A0 = 7938 / 18608
        Me._A1 = 9240 / 18608
        Me._A2 = 1430 / 18608
        Me._A3 = 0.0R
    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if This method releases both managed and unmanaged 
    '''   resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    ' onDisposeManagedResources

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary>Gets or sets the zero-order (offset) coefficient of the cosine window.</summary>
    Public Property A0() As Double

    ''' <summary>Gets or sets the first-order (cosine(2a)) coefficient of the cosine window.</summary>
    Public Property A1() As Double

    ''' <summary>Gets or sets the second-order (cosine(4a)) coefficient of the cosine window.</summary>
    Public Property A2() As Double

    ''' <summary>Gets or sets the third-order (cosine(6a)) coefficient of the cosine window.</summary>
    Public Property A3() As Double

    ''' <summary>Gets or sets the length of the window last created. Used 
    '''   by inherited classes to set the base phase.</summary>
    Protected Overrides Property ElementCount() As Integer
        Get
            Return MyBase.ElementCount
        End Get
        Set(ByVal Value As Integer)
            MyBase.ElementCount = Value
            MyBase.BasePhase = 2 * Math.PI / Convert.ToDouble(Value - 1)
        End Set
    End Property

    ''' <summary> Calculates the Blackman taper window amplitude for the provided element. The phase is
    ''' calculated as 2 * PI * i /(N - 1), i = 0 to N-1. </summary>
    ''' <param name="element"> The element number (i) for calculating the phase. </param>
    ''' <returns> The amplitude for the provided element. </returns>
    Public Overloads Overrides Function Amplitude(ByVal element As Integer) As Double

        Dim cos2a As Double = Math.Cos(MyBase.BasePhase * element)
        Dim cos4a As Double = 2.0R * cos2a * cos2a - 1.0R
        Dim cos6a As Double = cos2a * (2.0R * cos4a - 1.0R)
        Return Me.A0 - Me.A1 * cos2a + Me.A2 * cos4a - Me.A3 * cos6a

    End Function

#End Region

End Class

''' <summary>Applies a Hamming taper window.</summary>
''' <remarks>The Hamming window implements a first order cosine window:
'''   A =  0.54 - 0.46 * cos2a 
'''   where a is the base angle Pi*k/N, k=0,1,...N-1.
''' </remarks>
''' <license>
''' (c) 1998 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class HammingTaperWindow
    Inherits TaperWindow

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs This class.</summary>
    Public Sub New()

        ' instantiate the base class
        MyBase.New(TaperWindowType.Hamming)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if This method releases both managed and unmanaged 
    '''   resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " METHODS AND PROPERTIES "

    ' Hamming Window coefficients
    Private Const a0 As Double = 0.54R
    Private Const a1 As Double = 0.46R

    ''' <summary>Gets or sets the length of the window last created. Used 
    '''   by inherited classes to set the base phase.</summary>
    Protected Overrides Property ElementCount() As Integer
        Get
            Return MyBase.ElementCount
        End Get
        Set(ByVal Value As Integer)
            MyBase.ElementCount = Value
            MyBase.BasePhase = 2 * Math.PI / Convert.ToDouble(Value - 1)
        End Set
    End Property

    ''' <summary> Calculates the Blackman taper window amplitude for the provided element. The phase is
    ''' calculated as 2 * PI * i /(N - 1), i = 0 to N-1. </summary>
    ''' <param name="element"> The element number (i) for calculating the phase. </param>
    ''' <returns> The amplitude for the provided element. </returns>
    Public Overloads Overrides Function Amplitude(ByVal element As Integer) As Double

        Return a0 - a1 * Math.Cos(MyBase.BasePhase * element)

    End Function

#End Region

End Class

''' <summary>Applies a Hanning taper window.</summary>
''' <remarks>The Hanning window implements a first order cosine window:
'''   A =  0.5 - 0.5 * cos2a 
'''   where a is the base angle Pi*k/N, k=0,1,...N-1.
''' </remarks>
''' <license>
''' (c) 1998 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class HanningTaperWindow
    Inherits TaperWindow

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs This class.</summary>
    Public Sub New()

        ' instantiate the base class
        MyBase.New(TaperWindowType.Hanning)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if This method releases both managed and unmanaged 
    '''   resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " METHODS AND PROPERTIES "

    ' Hanning Window coefficients
    Private Const a0 As Double = 0.5R
    Private Const a1 As Double = 0.5R

    ''' <summary>Gets or sets the length of the window last created. Used 
    '''   by inherited classes to set the base phase.</summary>
    Protected Overrides Property ElementCount() As Integer
        Get
            Return MyBase.ElementCount
        End Get
        Set(ByVal Value As Integer)
            MyBase.ElementCount = Value
            MyBase.BasePhase = 2 * Math.PI / Convert.ToDouble(Value - 1)
        End Set
    End Property

    ''' <summary> Calculates the Blackman taper window amplitude for the provided element. The phase is
    ''' calculated as 2 * PI * i /(N - 1), i = 0 to N-1. </summary>
    ''' <param name="element"> The element number (i) for calculating the phase. </param>
    ''' <returns> The amplitude for the provided element. </returns>
    Public Overloads Overrides Function Amplitude(ByVal element As Integer) As Double

        Return a0 - a1 * Math.Cos(MyBase.BasePhase * element)

    End Function

#End Region

End Class

''' <summary>
''' Enumerates the taper windows types.
''' </summary>
Public Enum TaperWindowType
    <System.ComponentModel.Description("None")> None
    <System.ComponentModel.Description("Bartlett")> Bartlett
    <System.ComponentModel.Description("Blackman")> Blackman
    <System.ComponentModel.Description("Cosine")> Cosine
    <System.ComponentModel.Description("Hamming")> Hamming
    <System.ComponentModel.Description("Hanning")> Hanning
    <System.ComponentModel.Description("Blackman-Harris")> BlackmanHarris
End Enum

