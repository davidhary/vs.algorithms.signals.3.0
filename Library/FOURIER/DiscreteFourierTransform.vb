Imports System.Numerics

''' <summary> Calculates the Discrete Fourier Transform. </summary>
''' <remarks> Includes procedures for calculating the forward and inverse DFT.<p>
''' Benchmarks:</p><p>
''' CPU     Data    Speed</p><p>
''' Score   Points  (ms)</p><p>
''' 7.1     1000    1</p><p>
''' 7.1     1024    1</p><p>
''' 7.1     8192    10</p> </remarks>
''' <license> (c) 1998 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class DiscreteFourierTransform

    Inherits FourierTransformBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New(FourierTransformType.Dft)
    End Sub

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged
    ''' resources;
    ''' False if this method releases only unmanaged resources. </param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources
            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Calculates the Forward Fourier Transform. </summary>
    ''' <remarks> Swap real and imaginary values to compute the inverse Fourier transform. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    ''' are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="values"> Takes the time series and returns the spectrum values. </param>
    Public Overloads Overrides Sub Forward(ByVal values() As Complex)

        If values Is Nothing Then
            Throw New System.ArgumentNullException("values")
        End If
        If values.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("values", "Array must be longer than 1")
        End If

        ' build the DFT tables
        MyBase.BuildDftTable(values.Length)

        ' Set element count
        MyBase.ElementCount = values.Length

        ' save initial values to the cache
        MyBase.CopyToCache(values)

        Dim value As Complex
        Dim k As Integer
        For j As Integer = 0 To MyBase.ElementCount - 1
            value = MyBase.DftCache(0)
            k = 0
            For i As Integer = 1 To MyBase.ElementCount - 1
                k += j
                If k >= MyBase.ElementCount Then
                    k -= MyBase.ElementCount
                End If
                value += MyBase.DftCache(i) * MyBase.DftTable(k).Conjugate
            Next i
            values(j) = value
        Next j

    End Sub

#End Region

End Class
