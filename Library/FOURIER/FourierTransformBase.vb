Imports System.Numerics

''' <summary> Provides a base class for fast and discrete Fourier transform calculations. </summary>
''' <license> (c) 1998 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public MustInherit Class FourierTransformBase
    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Specialized constructor for use only by derived classes. </summary>
    ''' <param name="transformType"> Type of the transform. </param>
    Protected Sub New(ByVal transformType As FourierTransformType)
        MyBase.New()
        Me._transformType = transformType
    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks> Do not make this method Overridable (virtual) because a derived class should not be
    ''' able to override this method. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> Gets or sets (private) the dispose status sentinel. </summary>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed() As Boolean

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks> Executes in two distinct scenarios as determined by its disposing parameter.  If True,
    ''' the method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed. </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    ''' False if this method releases only unmanaged resources. </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources
                Me._DftCache = Nothing
                Me._DftTable = Nothing

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> Number of elements. </summary>
    Private _elementCount As Integer

    ''' <summary> Gets or sets the local size of the time series arrays used for calculating the
    ''' Fourier transform.  This value is held separately allowing the first call to determine if any
    ''' cache needs to be recalculated in case of the time series length is different from the
    ''' previous calculation. Setting the time series length also sets the
    ''' <see cref="HalfSpectrumLength">length of the half spectrum</see>. </summary>
    ''' <value> The number of elements. </value>
    Protected Overridable Property ElementCount() As Integer
        Get
            Return Me._elementCount
        End Get
        Set(ByVal Value As Integer)
            Me._elementCount = Value
            Me._timeSeriesLength = Value
            Me._halfSpectrumLength = CInt(Math.Floor(Me.TimeSeriesLength / 2) + 1)
        End Set
    End Property

    ''' <summary> Length of the half spectrum. </summary>
    Private _halfSpectrumLength As Integer

    ''' <summary> The number of elements in half the spectrum. </summary>
    ''' <value> The length of the half spectrum. </value>
    Public ReadOnly Property HalfSpectrumLength() As Integer
        Get
            Return Me._halfSpectrumLength
        End Get
    End Property

    ''' <summary> Length of the time series. </summary>
    Private _timeSeriesLength As Integer

    ''' <summary> Returns the number of elements in the time series sample that was used to compute the
    ''' Fourier transform. </summary>
    ''' <value> The length of the time series. </value>
    Public ReadOnly Property TimeSeriesLength() As Integer
        Get
            Return Me._timeSeriesLength
        End Get
    End Property

    ''' <summary> Type of the transform. </summary>
    Private _transformType As FourierTransformType

    ''' <summary> Gets the Fourier Transform type. </summary>
    ''' <value> The type of the transform. </value>
    Public ReadOnly Property TransformType() As FourierTransformType
        Get
            Return Me._transformType
        End Get
    End Property

#End Region

#Region " COMPLEX "

    ''' <summary> Calculates the Forward Fourier Transform. </summary>
    ''' <remarks> Swap reals and imaginaries to computer the inverse Fourier transform. </remarks>
    ''' <param name="values"> Takes the time series and returns the spectrum values. </param>
    Public Overridable Sub Forward(ByVal values() As Complex)
        ' Initialize if new transform.  Move out to the calling methods
        Me.Initialize(values)
    End Sub

    ''' <summary> Initializes the transform. </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    ''' are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="values"> Holds the inputs and returns the outputs. </param>
    Public Overridable Sub Initialize(ByVal values() As Complex)

        If values Is Nothing Then
            Throw New System.ArgumentNullException("values")
        End If
        If values.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("values", "Array must be longer than 1")
        End If

        ' set the element count
        Me.ElementCount = values.Length

    End Sub

    ''' <summary> Calculates the Inverse Fourier Transform. </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    ''' are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="values"> Takes the spectrum values and returns the time series. </param>
    Public Sub Inverse(ByVal values() As Complex)

        If values Is Nothing Then
            Throw New System.ArgumentNullException("values")
        End If
        If values.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("values", "Array must be longer than 1")
        End If
        values.Swap()
        Me.Forward(values)
        values.Swap()

    End Sub

#End Region

#Region " PROTECTED "

    ''' <summary> The DFT table. </summary>
    Private _DftTable() As Complex

    ''' <summary> Returns the complex DFT table. </summary>
    ''' <returns> The complex DFT table. </returns>
    Protected Function DftTable() As Complex()
        Return Me._DftTable
    End Function

    ''' <summary> The DFT cache. </summary>
    Private _DftCache() As Complex

    ''' <summary> Returns the cache of DFT values. </summary>
    ''' <returns> The complex cache of the DFT table. </returns>
    Protected Function DftCache() As Complex()
        Return Me._DftCache
    End Function

    ''' <summary> Size of the DFT tables. </summary>
    Private _dftTablesSize As Integer

    ''' <summary> Builds sine and cosine tables and allocate cache for calculating the DFT.  These DFT
    ''' tables start at Delta and end at 360 degrees and are suitable for calculating the DFT for the
    ''' Mixed Radix Algorithm. </summary>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="elementCount"> Specifies the size of the sine tables. </param>
    Protected Sub BuildShiftedDftTable(ByVal elementCount As Integer)

        ' throw an exception if size not right
        If elementCount <= 1 Then
            Throw New ArgumentOutOfRangeException("elementCount", elementCount, "Must be greater than 1")
        End If

        ' exit if sine tables already constructed
        If Me._dftTablesSize = elementCount Then
            Return
        End If

        ' store the new size
        Me._dftTablesSize = elementCount

        ' allocate DFT cache and tables 
        ReDim _DftTable(elementCount - 1)
        ReDim _DftCache(elementCount - 1)

        ' cosine and sine factors
        Dim delta As Double = 2 * Math.PI / Convert.ToDouble(elementCount)
        Dim cosineDelta As Double = Math.Cos(delta)
        Dim sineDelta As Double = Math.Sin(delta)

        ' Sine and cosine temporary values
        Dim sineAlpha As Double
        Dim cosineAlpha As Double

        ' start at last element 
        Dim beta As Integer = elementCount - 1

        ' Set last element of Cosine table to 1.0
        Dim cosineBeta As Double = 1.0#

        ' set last element of Sine table to 0.0
        Dim sineBeta As Double = 0.0#
        DftTable(beta) = New Complex(cosineBeta, sineBeta)

        ' Use trigonometric relationships to build the tables
        Dim alpha As Integer = 0
        Do
            cosineAlpha = cosineBeta * cosineDelta + sineBeta * sineDelta
            sineAlpha = cosineBeta * sineDelta - sineBeta * cosineDelta
            cosineBeta = cosineAlpha
            sineBeta = -sineAlpha

            DftTable(alpha) = New Complex(cosineAlpha, sineAlpha)

            beta -= 1
            DftTable(beta) = New Complex(cosineBeta, sineBeta)

            alpha += 1

        Loop While alpha < beta

    End Sub

    ''' <summary> Builds sine and cosine tables for calculating the DFT. These DFT tables start at zero
    ''' and end at 360 - delta degrees and are suitable for calculating the Discrete Fourier
    ''' transform and the sliding FFT. </summary>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="elementCount"> Specifies the size of the sine tables. </param>
    Protected Sub BuildDftTable(ByVal elementCount As Integer)

        ' throw an exception if size not right
        If elementCount <= 1 Then
            Throw New ArgumentOutOfRangeException("elementCount", elementCount, "Must be greater than 1")
        End If

        ' exit if sine tables already constructed
        If Me._dftTablesSize = elementCount Then
            Return
        End If

        ' store the new size
        Me._dftTablesSize = elementCount

        ' allocate the sine and cosine tables 
        ReDim _DftTable(elementCount - 1)

        ' cosine and sine factors
        Dim delta As Double = 2 * Math.PI / elementCount
        Dim cosineDelta As Double = Math.Cos(delta)
        Dim sineDelta As Double = Math.Sin(delta)

        ' start at the first element
        Dim alpha As Integer = 0
        Dim beta As Integer = elementCount

        ' Sine and cosine temporary values
        Dim sineAlpha As Double = 0.0R
        Dim cosineAlpha As Double = 1.0R
        Dim cosineBeta As Double
        Dim sineBeta As Double

        ' Set first element of Cosine table to 1.0
        ' set first element of Sine table to 0.0
        DftTable(alpha) = New Complex(cosineAlpha, sineAlpha)

        ' Use trigonometric and inverse relationships to
        ' build the tables
        Do
            ' Get the next angle for the left end size, which
            ' equals the current angle plus the base angle
            cosineBeta = cosineAlpha * cosineDelta - sineAlpha * sineDelta
            sineBeta = cosineAlpha * sineDelta + sineAlpha * cosineDelta

            ' Store the new values
            cosineAlpha = cosineBeta
            sineAlpha = sineBeta

            ' Save the next angle on the left-end
            alpha += 1
            DftTable(alpha) = New Complex(cosineAlpha, sineAlpha)

            ' Save the right-end elements as the mirror image
            ' of the left end elements
            beta -= 1
            DftTable(beta) = New Complex(cosineBeta, -sineBeta)

        Loop While alpha < beta

    End Sub

    ''' <summary> Copies the real- and imaginary-parts to the cache. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> The values. </param>
    Protected Sub CopyToCache(ByVal values() As Complex)

        If values Is Nothing Then
            Throw New System.ArgumentNullException("values")
        End If

        ' copies arrays to the cache
        ReDim _DftCache(values.Length - 1)
        values.CopyTo(Me._DftCache)

    End Sub

#End Region

End Class
