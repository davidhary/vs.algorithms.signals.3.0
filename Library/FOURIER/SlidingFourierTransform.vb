Imports System.Numerics

''' <summary> Calculate the Sliding Fourier Transform. </summary>
''' <license> (c) 1998 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class SlidingFourierTransform
    Inherits FourierTransformBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        MyBase.New(FourierTransformType.Sliding)
    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Initializes the sine and cosine tables for calculating the sliding Fourier transform. </summary>
    ''' <remarks> Allocates and calculates sine and cosine tables for the sliding Fourier transform and
    ''' maps the data arrays to the internal memory space of the sliding Fourier transform. </remarks>
    ''' <param name="values"> Holds the inputs and returns the outputs. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="0",
        Justification:="Validated in base class")>
    Public Overloads Overrides Sub Initialize(ByVal values() As Complex)

        If values IsNot Nothing Then
            MyBase.Initialize(values)

            ' save the real- and imaginary-parts into the cache
            MyBase.CopyToCache(values)

            ' Allocate the sine tables.
            MyBase.BuildDftTable(values.Length)
        End If

    End Sub

    ''' <summary> Calculates the sliding Fast Fourier Transform. </summary>
    ''' <param name="newValue"> The new value to use in calculating the sliding Fourier transform. </param>
    ''' <param name="oldValue"> The first real value in the time series previously used to calculate
    ''' the Fourier transform. </param>
    Public Sub Update(ByVal newValue As Complex, ByVal oldValue As Complex)

        ' get the difference term
        Dim delta As Complex = newValue - oldValue

        ' Update the sliding Fourier transform
        For i As Integer = 0 To MyBase.DftCache.Length - 1
            MyBase.DftCache(i) = MyBase.DftTable(i) * (MyBase.DftCache(i) + delta)
        Next i

    End Sub

#End Region

End Class
