Imports System.Numerics
Imports System.Diagnostics.CodeAnalysis

''' <summary> Calculates the Fast Fourier Transform using the Meta Numerics Library. </summary>
''' <remarks> Includes procedures for calculating the forward and inverse Fourier transform. </remarks>
''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="1/20/2014" by="David" revision=""> Created. </history>
Public Class MixedRadixFourierTransform
    Inherits FourierTransformBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New(FourierTransformType.MixedRadix)
    End Sub

#End Region

#Region " COMPLEX "

    ''' <summary> Calculates a Mixed-Radix Fourier transform. </summary>
    ''' <remarks> Swap reals and imaginaries to computer the inverse Fourier transform. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    ''' are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="values"> Takes the time series and returns the spectrum values. </param>
    Public Overloads Overrides Sub Forward(ByVal values() As Complex)

        If values Is Nothing Then
            Throw New System.ArgumentNullException("values")
        End If
        If values.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("values", "Array must be longer than 1")
        End If

        Dim ft As FourierTransformer = New FourierTransformer(values.Length)
        ft.Transform(values).CopyTo(values)

    End Sub

#End Region

End Class

