﻿Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Core.Diagnosis.TraceEventIds.IsrAlgorithmsSignalsLibraryTester

        Public Const AssemblyTitle As String = "Algorithms Signals Library Tester"
        Public Const AssemblyDescription As String = "Algorithms Signals Library Tester"
        Public Const AssemblyProduct As String = "Algorithms.Signals.Library.Tester.2014"

    End Class

End Namespace

