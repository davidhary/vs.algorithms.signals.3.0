Imports System.Numerics
Imports isr.Core.Primitives.StringNumericExtensions

''' <summary> Includes test program for calculating the spectrum using DFT, Mixed Radix, and
''' sliding Fourier transform algorithms. </summary>
''' <remarks> Launch this form by calling its Show or ShowDialog method from its default instance. </remarks>
''' <license> (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class SpectrumPanel
    Inherits isr.Core.Diagnosis.FormBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions

        ' This method is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets or sets the selected example. </summary>
    ''' <value> The selected example. </value>
    Private ReadOnly Property SelectedExample() As Example
        Get
            Return CType(CType(Me._ExampleComboBox.SelectedItem, System.Collections.Generic.KeyValuePair(Of [Enum], String)).Key, Example)
        End Get
    End Property

    ''' <summary> Gets or sets the selected SignalType. </summary>
    ''' <value> The type of the selected signal. </value>
    Private ReadOnly Property SelectedSignalType() As SignalType
        Get
            Return CType(CType(Me._SignalComboBox.SelectedItem, System.Collections.Generic.KeyValuePair(Of [Enum], String)).Key, SignalType)
        End Get
    End Property

    ''' <summary> Gets or sets the selected TaperFilterType. </summary>
    ''' <value> The type of the selected taper filter. </value>
    Private ReadOnly Property SelectedTaperFilterType() As isr.Algorithms.Signals.TaperFilterType
        Get
            Return CType(CType(Me._FilterComboBox.SelectedItem, System.Collections.Generic.KeyValuePair(Of [Enum], String)).Key, TaperFilterType)
        End Get
    End Property

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> The instantiated. </value>
    Friend Shared ReadOnly Property Instantiated() As Boolean
        Get
            Return My.Forms.SpectrumPanel IsNot Nothing AndAlso Not My.Forms.SpectrumPanel.IsDisposed
        End Get
    End Property

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary> Occurs before the form is closed. </summary>
    ''' <remarks> Use this method to optionally cancel the closing of the form. Because the form is not
    ''' yet closed at this point, this is also the best place to serialize a form's visible
    ''' properties, such as size and location. Finally, dispose of any form level objects especially
    ''' those that might needs access to the form and thus should not be terminated after the form
    ''' closed. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      <see cref="System.ComponentModel.CancelEventArgs"/> </param>
    Private Sub form_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        ' disable the timer if any
        ' actionTimer.Enabled = False
        My.Application.DoEvents()

        ' set module objects that reference other objects to Nothing

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            ' terminate form-level objects
            ' Me.terminateObjects()
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try

    End Sub

    ''' <summary> Occurs when the form is loaded. </summary>
    ''' <remarks> Use this method for doing any final initialization right before the form is shown.
    ''' This is a good place to change the Visible and ShowInTaskbar properties to start the form as
    ''' hidden.  
    ''' Starting a form as hidden is useful for forms that need to be running but that should not
    ''' show themselves right away, such as forms with a notify icon in the task bar. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      <see cref="System.EventArgs"/> </param>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' instantiate form objects
            'Me.instantiateObjects()

            ' set the form caption
            Me.Text = My.Application.Info.BuildDefaultCaption(": SPECTRUM PANEL")

            ' Initialize and set the user interface
            initializeUserInterface()

            ' center the form
            Me.CenterToScreen()

            ' turn on the loaded flag
            ' loaded = True

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

#Region " PRIVATE  and  PROTECTED "

    ''' <summary> Shows the status. </summary>
    ''' <param name="message"> The message. </param>
    Private Sub showStatus(ByVal message As String)

        Me._MessagesList.AddMessage(message)
        Me._StatusToolStripStatusLabel.Text = message

    End Sub

    ''' <summary>Gets or sets the data format</summary>
    Const listFormat As String = "{0:0.000000000000000}{1}{2:0.000000000000000}"

    ''' <summary> Enumerates the action options. </summary>
    Private Enum Example
        ''' <summary> . </summary>
        <System.ComponentModel.Description("Discrete Fourier Transform")> DiscreteFourierTransform
        ''' <summary> . </summary>
        <System.ComponentModel.Description("Sliding FFT")> SlidingFFT
        ''' <summary> . </summary>
        <System.ComponentModel.Description("Mixed Radix FFT")> MixedRadixFFT
    End Enum

    ''' <summary> Enumerates the signal options. </summary>
    Private Enum SignalType
        ''' <summary> . </summary>
        <System.ComponentModel.Description("Sine")> SineWave
        ''' <summary> . </summary>
        <System.ComponentModel.Description("Random")> Random
    End Enum

    ''' <summary> Initializes the user interface and tool tips. </summary>
    ''' <remarks> Call this method from the form load method to set the user interface. </remarks>
    Private Sub initializeUserInterface()

        ' tipsToolTip.SetToolTip(Me.txtDuration, "Enter count-down duration in seconds")
        ' tipsToolTip.SetToolTip(Me.exitButton, "Click to exit")

        ' Set initial values defining the signal
        Me._PointsTextBox.Text = "1000"
        Me._CyclesTextBox.Text = "1" ' "11.5"
        Me._PhaseTextBox.Text = "0" ' "45"
        Me._PointsToDisplayTextBox.Text = "100"
        Me._SignalDurationTextBox.Text = "1"

        ' set the default sample
        Me.populateComboBoxs()

        ' create the two charts.
        createSpectrumChart()
        createSignalChart()

        ' plot the signal
        Me.UpdateSignal()

    End Sub

    ''' <summary> Updates the signal. </summary>
    Private Sub UpdateSignal()
        If Me._DoubleRadioButton.Checked Then
            UpdateSignalDouble()
        Else
            UpdateSignalSingle()
        End If
    End Sub

    ''' <summary> Gets the signal Single. </summary>
    ''' <returns> The signal. </returns>
    Private Function GetSignalSingle() As Single()

        Dim signal As Single()
        Select Case SelectedSignalType
            Case SignalType.Random
                signal = RandomWaveSingle()
            Case Else
                signal = SineWaveSingle()
        End Select
        Return signal

    End Function

    ''' <summary> Updates the signal single. </summary>
    ''' <returns> A new signal. </returns>
    Private Function UpdateSignalSingle() As Single()

        Dim signal As Single() = GetSignalSingle()
        ' Plot the Signal 
        Dim duration As Single = Single.Parse(Me._SignalDurationTextBox.Text, Globalization.CultureInfo.CurrentCulture)
        Dim frequences As Single() = isr.Algorithms.Signals.Signal.Ramp(duration / signal.Length, signal.Length)
        Me.chartSignal(frequences, signal)
        Return signal

    End Function

    ''' <summary> Gets the signal double. </summary>
    ''' <returns> The signal. </returns>
    Private Function GetSignalDouble() As Double()

        Dim signal As Double()
        Select Case SelectedSignalType
            Case SignalType.Random
                signal = RandomWaveDouble()
            Case Else
                signal = SineWaveDouble()
        End Select
        Return signal

    End Function

    ''' <summary> Updates the signal double. </summary>
    ''' <returns> The signal. </returns>
    Private Function UpdateSignalDouble() As Double()

        Dim signal As Double() = GetSignalDouble()
        ' Plot the Signal 
        Dim duration As Double = Double.Parse(Me._SignalDurationTextBox.Text, Globalization.CultureInfo.CurrentCulture)
        Dim frequences As Double() = isr.Algorithms.Signals.Signal.Ramp(duration / signal.Length, signal.Length)
        Me.chartSignal(frequences, signal)
        Return signal

    End Function

    ''' <summary> Calculates the sine wave. </summary>
    ''' <returns> The sine wave signal. </returns>
    Private Function SineWaveDouble() As Double()

        ' Read number of points from the text box.
        Dim signalPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create cycles of the sine wave.
        Dim signalCycles As Double = Double.Parse(Me._CyclesTextBox.Text, Globalization.CultureInfo.CurrentCulture)
        Dim signalPhase As Double = isr.Algorithms.Signals.Signal.ToRadians(Double.Parse(Me._PhaseTextBox.Text, Globalization.NumberStyles.Any, Globalization.CultureInfo.CurrentCulture))
        ' get the signal
        Return isr.Algorithms.Signals.Signal.Sine(signalCycles, signalPhase, signalPoints)
    End Function

    ''' <summary> Calculates the sine wave. </summary>
    ''' <returns> The sine wave signal. </returns>
    Private Function SineWaveSingle() As Single()

        ' Read number of points from the text box.
        Dim signalPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create cycles of the sine wave.
        Dim signalCycles As Single = Single.Parse(Me._CyclesTextBox.Text, Globalization.CultureInfo.CurrentCulture)
        Dim signalPhase As Double = isr.Algorithms.Signals.Signal.ToRadians(Double.Parse(Me._PhaseTextBox.Text, Globalization.NumberStyles.Any, Globalization.CultureInfo.CurrentCulture))
        ' get the signal
        Return isr.Algorithms.Signals.Signal.Sine(signalCycles, CSng(signalPhase), signalPoints)
    End Function

    ''' <summary> Calculates the Random wave. </summary>
    ''' <returns> The random signal. </returns>
    Private Function RandomWaveDouble() As Double()

        ' Read number of points from the text box.
        Dim dataPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)
        Return isr.Algorithms.Signals.Signal.Random(1.0R + DateTime.Now.Second, dataPoints)
    End Function

    ''' <summary> Calculates the Random wave. </summary>
    ''' <returns> The random signal. </returns>
    Private Function RandomWaveSingle() As Single()

        ' Read number of points from the text box.
        Dim dataPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)
        Dim signal() As Single = isr.Algorithms.Signals.Signal.Random(1.0F + DateTime.Now.Second, dataPoints)
        Return signal

    End Function

    ''' <summary> The copy signal format. </summary>
    Private Const copySigFormat As String = "    Copy Signal:  {0:0.###} ms "

    ''' <summary> The FFT initialize format. </summary>
    Private Const fftInitFormat As String = " FFT Initialize:  {0:0} ms "

    ''' <summary> The FFT calculate format. </summary>
    Private Const fftCalcFormat As String = "  FFT Calculate:  {0:0.###} ms "

    ''' <summary> The frequency calculate format. </summary>
    Private Const frqCalcFormat As String = "FFT Frequencies:  {0:0.###} ms "

    ''' <summary> The magnitude calculate format. </summary>
    Private Const magCalcFormat As String = "  FFT Magnitude:  {0:0.###} ms "

    ''' <summary> The inverse calculate format. </summary>
    Private Const invCalcFormat As String = "    Inverse FFT:  {0:0.###} ms "

    ''' <summary> The charting format. </summary>
    Private Const chartingFormat As String = "  Charting time:  {0:0} ms "

    ''' <summary> Calculates the Spectrum based on the selected algorithm and displays the results. </summary>
    ''' <param name="algorithm"> The algorithm. </param>
    Private Sub calculateSpectrumDouble(ByVal algorithm As Example)

        Dim timeKeeper As Diagnostics.Stopwatch
        timeKeeper = New Diagnostics.Stopwatch
        Dim duration As TimeSpan
        Me._TimingTextBox.Text = isr.Core.Primitives.EnumExtensions.Description(algorithm)

        showStatus("Calculating double-precision " & algorithm.ToString & " FFT")

        ' ------------ Create the Signal for FFT ---------------

        ' Set FFT points 
        Dim fftPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create and plot the signal
        Dim signal() As Double = Me.UpdateSignalDouble

        ' Allocate the spectrum array.
        Dim fftValues(fftPoints - 1) As Complex
        signal.CopyTo(fftValues)

        ' ------------ SELECT PROPERTIES ---------------

        ' select the spectrum type
        Dim fft As isr.Algorithms.Signals.FourierTransformBase = Nothing
        Try

            Select Case algorithm
                Case Example.DiscreteFourierTransform
                    fft = New isr.Algorithms.Signals.DiscreteFourierTransform
                Case Example.MixedRadixFFT
                    fft = New isr.Algorithms.Signals.MixedRadixFourierTransform
                Case Else
                    Return
            End Select
            Using spectrum As isr.Algorithms.Signals.Spectrum = New isr.Algorithms.Signals.Spectrum(fft)


                ' scale the FFT by the Window power and data points
                spectrum.IsScaleFft = True

                ' Remove mean before calculating the FFT
                spectrum.IsRemoveMean = Me._RemoveMeanCheckBox.Checked

                ' Use Taper Window as selected
                If Me._TaperWindowCheckBox.Checked Then
                    spectrum.TaperWindow = New isr.Algorithms.Signals.BlackmanTaperWindow
                Else
                    spectrum.TaperWindow = Nothing
                End If

                Const transitionBand As Double = 0.01
                Const lowFreq As Double = 0.2
                Const highFreq As Double = 0.4
                spectrum.TaperFilter = New isr.Algorithms.Signals.TaperFilter(Me.SelectedTaperFilterType)
                spectrum.SamplingRate = fftPoints
                spectrum.TaperFilter.TimeSeriesLength = fftPoints
                spectrum.TaperFilter.LowFrequency = spectrum.SamplingRate * lowFreq
                spectrum.TaperFilter.HighFrequency = spectrum.SamplingRate * highFreq
                spectrum.TaperFilter.TransitionBand = spectrum.SamplingRate * transitionBand
                Select Case Me.SelectedTaperFilterType
                    Case TaperFilterType.BandPass
                    Case TaperFilterType.BandReject
                    Case TaperFilterType.HighPass
                    Case TaperFilterType.LowPass
                    Case Else
                End Select

                ' Initialize the FFT.
                timeKeeper.Restart()
                spectrum.Initialize(fftValues)
                duration = timeKeeper.Elapsed
                Me._TimingTextBox.Text = Me._TimingTextBox.Text & Environment.NewLine &
                    String.Format(Globalization.CultureInfo.CurrentCulture, fftInitFormat, duration.TotalMilliseconds)

                ' ------------ CREATE the SIGNAL ---------------

                timeKeeper.Restart()
                signal.CopyTo(fftValues)
                duration = timeKeeper.Elapsed
                Me._TimingTextBox.Text = Me._TimingTextBox.Text & Environment.NewLine &
                    String.Format(Globalization.CultureInfo.CurrentCulture, copySigFormat, duration.TotalMilliseconds)

                ' ------------ Calculate Forward and Inverse FFT ---------------

                ' Compute the FFT.
                timeKeeper.Restart()
                spectrum.Calculate(fftValues)
                duration = timeKeeper.Elapsed
                Me._TimeToolStripStatusLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                  "{0:0.###} ms", duration.TotalMilliseconds)
                Me._TimingTextBox.Text = Me._TimingTextBox.Text & Environment.NewLine &
                    String.Format(Globalization.CultureInfo.CurrentCulture, fftCalcFormat, duration.TotalMilliseconds)

                ' ------- Calculate FFT outcomes ---------

                ' You must re-map the FFT utilities if using more than one time series at a time
                ' isr.Algorithms.Signals.Helper.Map(real, imaginary)

                ' Get the FFT magnitudes
                Dim magnitudes() As Double
                magnitudes = isr.Algorithms.Signals.ComplexArrays.Magnitudes(fftValues)

                timeKeeper.Restart()
                magnitudes = isr.Algorithms.Signals.ComplexArrays.Magnitudes(fftValues)
                duration = timeKeeper.Elapsed
                Me._TimingTextBox.Text = Me._TimingTextBox.Text & Environment.NewLine &
                    String.Format(Globalization.CultureInfo.CurrentCulture, magCalcFormat, duration.TotalMilliseconds)

                ' Get the Frequency
                Dim frequencies() As Double

                timeKeeper.Restart()
                frequencies = isr.Algorithms.Signals.Signal.Frequencies(1.0R, fftPoints)
                duration = timeKeeper.Elapsed
                Me._TimingTextBox.Text = Me._TimingTextBox.Text & Environment.NewLine &
                    String.Format(Globalization.CultureInfo.CurrentCulture, frqCalcFormat, duration.TotalMilliseconds)

                ' Compute the inverse transform.
                timeKeeper.Restart()
                fft.Inverse(fftValues)
                duration = timeKeeper.Elapsed
                Me._TimingTextBox.Text = Me._TimingTextBox.Text & Environment.NewLine &
                    String.Format(Globalization.CultureInfo.CurrentCulture, invCalcFormat, duration.TotalMilliseconds)

                ' Display the forward and inverse data.
                DisplayCalculationAccuracy(signal, fftValues)

                ' ------------ Plot FFT outcome ---------------
                timeKeeper.Restart()
                chartAmplitudeSpectrum(frequencies, magnitudes, 1)
                duration = timeKeeper.Elapsed
                Me._TimingTextBox.Text = Me._TimingTextBox.Text & Environment.NewLine &
                    String.Format(Globalization.CultureInfo.CurrentCulture, chartingFormat, duration.TotalMilliseconds)
            End Using
        Catch
            Throw
        Finally
            If fft IsNot Nothing Then
                fft.Dispose()
                fft = Nothing
            End If
        End Try

    End Sub

    ''' <summary> Calculates the Spectrum based on the selected algorithm and displays the results. </summary>
    ''' <param name="algorithm"> The algorithm. </param>
    Private Sub calculateSpectrumSingle(ByVal algorithm As Example)

        Dim timeKeeper As Diagnostics.Stopwatch
        timeKeeper = New Diagnostics.Stopwatch
        Dim duration As TimeSpan

        showStatus("Calculating single-precision " & algorithm.ToString & " FFT")

        ' ------------ Create the Signal for FFT ---------------

        ' Set FFT points 
        Dim fftPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create and plot the signal
        Dim signal() As Single = Me.UpdateSignalSingle

        ' Allocate the spectrum array.
        Dim fftValues(fftPoints - 1) As Complex
        signal.CopyTo(fftValues)

        ' ------------ Calculate Forward and Inverse FFT ---------------

        Dim fft As isr.Algorithms.Signals.FourierTransformBase = Nothing
        Try
            Select Case algorithm
                Case Example.DiscreteFourierTransform
                    fft = New isr.Algorithms.Signals.DiscreteFourierTransform
                Case Example.MixedRadixFFT
                    fft = New isr.Algorithms.Signals.MixedRadixFourierTransform
                Case Else
                    Return
            End Select
            Using spectrum As isr.Algorithms.Signals.Spectrum = New isr.Algorithms.Signals.Spectrum(fft)

                ' scale the FFT by the Window power and data points
                spectrum.IsScaleFft = True

                ' Remove mean before calculating the FFT
                spectrum.IsRemoveMean = Me._RemoveMeanCheckBox.Checked

                ' Use Taper Window as selected
                If Me._TaperWindowCheckBox.Checked Then
                    spectrum.TaperWindow = New isr.Algorithms.Signals.BlackmanTaperWindow
                Else
                    spectrum.TaperWindow = Nothing
                End If

                ' Initialize the FFT.
                timeKeeper.Restart()
                spectrum.Initialize(fftValues)
                duration = timeKeeper.Elapsed
                Me._TimingTextBox.Text = Me._TimingTextBox.Text & Environment.NewLine &
                    String.Format(Globalization.CultureInfo.CurrentCulture, fftInitFormat, duration.TotalMilliseconds)

                ' Compute the FFT.
                timeKeeper.Restart()
                spectrum.Calculate(fftValues)
                duration = timeKeeper.Elapsed

                ' Display time
                Me._TimeToolStripStatusLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "{0:0.###} ms ",
                                                              duration.TotalMilliseconds)

                ' ------- Calculate FFT outcomes ---------

                ' You must re-map the FFT utilities if using more than one time series at a time
                ' isr.Algorithms.Signals.Helper.Map(real, imaginary)

                ' Get the FFT magnitudes
                Dim magnitudes() As Double
                magnitudes = isr.Algorithms.Signals.ComplexArrays.Magnitudes(fftValues)

                ' Get the Frequency
                Dim frequencies() As Double
                frequencies = isr.Algorithms.Signals.Signal.Frequencies(1.0R, fftPoints)

                ' Compute the inverse transform.
                fft.Inverse(fftValues)

                ' Display the forward and inverse data.
                DisplayCalculationAccuracy(signal, fftValues)

                ' ------------ Plot FFT outcome ---------------
                chartAmplitudeSpectrum(frequencies, magnitudes, 1)
            End Using
        Catch
            Throw
        Finally
            If fft IsNot Nothing Then
                fft.Dispose()
                fft = Nothing
            End If
        End Try

    End Sub

    ''' <summary> Displays the test results. </summary>
    ''' <param name="signal">     The signal. </param>
    ''' <param name="timeSeries"> The time series. </param>
    Private Sub DisplayCalculationAccuracy(ByVal signal() As Double, ByVal timeSeries() As Complex)

        If signal Is Nothing Then
            Exit Sub
        End If
        If timeSeries Is Nothing Then
            Exit Sub
        End If
        Dim elementCount As Integer = Math.Min(timeSeries.Length, Integer.Parse(Me._PointsToDisplayTextBox.Text, Globalization.CultureInfo.CurrentCulture))
        If elementCount <= 0 Then
            Exit Sub
        End If

        ' Display the forward and inverse data.
        Dim totalError As Double = 0
        Me._SignalListBox.Items.Clear()
        Dim value As Double
        For i As Integer = 0 To elementCount - 1
            value = signal(i) - timeSeries(i).Real
            totalError += value * value
            Me._SignalListBox.Items.Add(String.Format(Globalization.CultureInfo.CurrentCulture,
                                               listFormat, signal(i), Microsoft.VisualBasic.ControlChars.Tab, timeSeries(i)))
        Next i
        totalError = System.Math.Sqrt(totalError / Convert.ToDouble(elementCount))
        Me._ErrorToolStripStatusLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "{0:0.###E+0}", totalError)
        Me._StatusStrip.Invalidate()

    End Sub

    ''' <summary> Displays the test results. </summary>
    ''' <param name="signal">     The signal. </param>
    ''' <param name="timeSeries"> The time series. </param>
    Private Sub displayCalculationAccuracy(ByVal signal() As Single, ByVal timeSeries() As Complex)

        If signal Is Nothing Then
            Exit Sub
        End If
        If timeSeries Is Nothing Then
            Exit Sub
        End If
        Dim elementCount As Integer = Math.Min(timeSeries.Length, Integer.Parse(Me._PointsToDisplayTextBox.Text, Globalization.CultureInfo.CurrentCulture))
        If elementCount <= 0 Then
            Exit Sub
        End If

        ' Display the forward and inverse data.
        Dim totalError As Double = 0
        Me._SignalListBox.Items.Clear()
        Dim value As Double
        For i As Integer = 0 To elementCount - 1
            value = signal(i) - timeSeries(i).Real
            totalError += value * value
            Me._SignalListBox.Items.Add(String.Format(Globalization.CultureInfo.CurrentCulture,
                                               listFormat, signal(i), Microsoft.VisualBasic.ControlChars.Tab, timeSeries(i)))
        Next i
        totalError = System.Math.Sqrt(totalError / Convert.ToDouble(elementCount))
        Me._ErrorToolStripStatusLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "{0:0.###E+0}", totalError)
        Me._StatusStrip.Invalidate()

    End Sub

    ''' <summary> Populates the list of options in the action combo box. </summary>
    ''' <remarks> It seems that out enumerated list does not work very well with this list. </remarks>
    Private Sub populateComboBoxs()

        ' set the action list
        Me._ExampleComboBox.Items.Clear()
        Me._ExampleComboBox.DataSource = isr.Core.Primitives.EnumExtensions.ValueDescriptionPairs(GetType(Example))
        Me._ExampleComboBox.DisplayMember = "Value"
        Me._ExampleComboBox.ValueMember = "Key"
        Me._ExampleComboBox.SelectedIndex = 0

        Me._SignalComboBox.Items.Clear()
        Me._SignalComboBox.DataSource = isr.Core.Primitives.EnumExtensions.ValueDescriptionPairs(GetType(SignalType))
        Me._SignalComboBox.DisplayMember = "Value"
        Me._SignalComboBox.ValueMember = "Key"
        Me._SignalComboBox.SelectedIndex = 0

        Me._FilterComboBox.Items.Clear()
        Me._FilterComboBox.DataSource = isr.Core.Primitives.EnumExtensions.ValueDescriptionPairs(GetType(isr.Algorithms.Signals.TaperFilterType))
        Me._FilterComboBox.DisplayMember = "Value"
        Me._FilterComboBox.ValueMember = "Key"
        Me._FilterComboBox.SelectedIndex = 0

    End Sub

    ''' <summary> Evaluates sliding FFT until stopped. </summary>
    ''' <param name="noiseFigure"> Specifies the relative noise level to add to the signal for
    ''' observing the changes in the spectrum. </param>
    Private Sub slidingFft(ByVal noiseFigure As Double)

        showStatus("Calculating double-precision sliding FFT")

        ' ------------ Create the Signal for FFT ---------------

        ' Set FFT points 
        Dim fftPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create cycles of the sine wave.
        Dim signal() As Double = Me.SineWaveDouble()

        ' Allocate the spectrum array.
        Dim fftValues(fftPoints - 1) As Complex
        signal.CopyTo(fftValues)

        ' ------------ Calculate Forward and Inverse FFT ---------------

        Dim duration As TimeSpan = TimeSpan.Zero
        ' Create a new instance of the Mixed Radix class
        Using fft As isr.Algorithms.Signals.MixedRadixFourierTransform = New isr.Algorithms.Signals.MixedRadixFourierTransform
            ' Compute the FFT.
            Dim timeKeeper As Diagnostics.Stopwatch
            timeKeeper = New Diagnostics.Stopwatch
            timeKeeper.Restart()
            fft.Forward(fftValues)
            duration = timeKeeper.Elapsed
        End Using

        ' Display time
        Me._TimeToolStripStatusLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                             "{0:0.###} ms",
                                                             duration.TotalMilliseconds)

        ' Clear the data
        Me._SignalListBox.Items.Clear()

        ' Clear the error value
        Me._ErrorToolStripStatusLabel.Text = String.Empty

        ' Get the FFT magnitudes
        Dim magnitudes() As Double
        magnitudes = isr.Algorithms.Signals.ComplexArrays.Magnitudes(fftValues)

        ' Get the Frequency
        Dim frequencies() As Double
        frequencies = isr.Algorithms.Signals.Signal.Frequencies(1.0R, fftPoints)

        ' Initialize the last point of the signal to the last point.
        Dim lastSignalPoint As Integer = fftPoints - 1

        ' Initialize the first point of the signal to the last point.
        Dim firstSignalPoint As Integer = 0

        ' Calculate the phase of the last point of the sine wave
        Dim deltaPhase As Double = 2 * Math.PI * Single.Parse(Me._CyclesTextBox.Text, Globalization.CultureInfo.CurrentCulture) / Convert.ToSingle(fftPoints)

        ' get the signal phase of the last point
        Dim signalPhase As Double = isr.Algorithms.Signals.Signal.ToRadians(Double.Parse(Me._PhaseTextBox.Text, Globalization.CultureInfo.CurrentCulture))
        signalPhase += deltaPhase * lastSignalPoint

        ' Use a new frequency for the sine wave to see how
        ' the old is phased out and the new gets in
        deltaPhase *= 2

        ' First element in the previous value of the time series
        Dim oldValue As Complex

        ' New value from the Signal
        Dim newValue As Complex

        ' Create a new instance of the sliding FFT class
        Using slidingFft As isr.Algorithms.Signals.SlidingFourierTransform = New isr.Algorithms.Signals.SlidingFourierTransform

            ' Initialize sliding FFT coefficients.
            slidingFft.Initialize(fftValues)

            ' Recalculate the sliding FFT
            Do While Me._StartStopCheckBox.Checked

                ' Allow other events to occur
                My.Application.DoEvents()

                ' ------- Calculate FFT outcomes ---------

                ' You must re-map the FFT utilities if using more than one time series at a time
                ' isr.Algorithms.Signals.Helper.Map(real, imaginary)

                ' Get the FFT magnitudes
                magnitudes = isr.Algorithms.Signals.ComplexArrays.Magnitudes(fftValues)

                ' Get the Frequency
                frequencies = isr.Algorithms.Signals.Signal.Frequencies(1.0R, fftPoints)

                ' ------------ Plot the Signal ---------------

                Me.chartSignal(isr.Algorithms.Signals.Signal.Ramp(Double.Parse(Me._SignalDurationTextBox.Text,
                                                                             Globalization.CultureInfo.CurrentCulture) / fftPoints, fftPoints), signal)

                ' ------------ Plot FFT outcome ---------------

                chartAmplitudeSpectrum(frequencies, magnitudes, 1)

                ' Update the previous values of the signal
                oldValue = New Complex(signal(firstSignalPoint), 0)

                ' Add some random noise to make it interesting.
                Dim noise As Double = noiseFigure * 2 * (Microsoft.VisualBasic.Rnd() - 0.5)

                ' Get new signal values.
                signalPhase += deltaPhase
                newValue = New Complex(System.Math.Sin(signalPhase) + noise, 0)

                ' Update the signal itself.

                ' Update the last point.
                lastSignalPoint += 1
                If lastSignalPoint >= fftPoints Then
                    lastSignalPoint = 0
                End If

                ' Update the first point.
                firstSignalPoint += 1
                If firstSignalPoint >= fftPoints Then
                    firstSignalPoint = 0
                End If

                ' Place new data in the signal itself.
                signal(lastSignalPoint) = newValue.Real

                ' You must re-map the FFT utilities if using more than one time series at a time
                ' isr.Algorithms.Signals.Helper.Map(real, imaginary)

                ' Calculate the sliding FFT coefficients.
                slidingFft.Update(newValue, oldValue)

            Loop
        End Using


    End Sub

    ''' <summary> Evaluates sliding FFT until stopped. </summary>
    ''' <param name="noiseFigure"> Specifies the relative noise level to add to the signal for
    ''' observing the changes in the spectrum. </param>
    Private Sub slidingFft(ByVal noiseFigure As Single)

        showStatus("Calculating single-precision sliding FFT")

        ' ------------ Create the Signal for FFT ---------------

        ' Set FFT points 
        Dim fftPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create and plot the signal
        Dim signal() As Single = Me.UpdateSignalSingle

        ' Allocate the spectrum array.
        Dim fftValues(fftPoints - 1) As Complex
        signal.CopyTo(fftValues)

        ' ------------ Calculate Forward and Inverse FFT ---------------

        ' Create a new instance of the Mixed Radix class
        Using fft As isr.Algorithms.Signals.MixedRadixFourierTransform = New isr.Algorithms.Signals.MixedRadixFourierTransform

            ' Compute the FFT.
            Dim timeKeeper As Diagnostics.Stopwatch
            timeKeeper = New Diagnostics.Stopwatch
            timeKeeper.Restart()
            fft.Forward(fftValues)
            Dim duration As TimeSpan = timeKeeper.Elapsed

            ' Display time
            Me._TimeToolStripStatusLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "{0:0.###} ms",
                duration.TotalMilliseconds)

            ' Clear the data
            Me._SignalListBox.Items.Clear()
            Me._ErrorToolStripStatusLabel.Text = String.Empty

            ' Get the FFT magnitudes
            Dim magnitudes() As Double
            magnitudes = isr.Algorithms.Signals.ComplexArrays.Magnitudes(fftValues)

            ' Get the Frequency
            Dim frequencies() As Double
            frequencies = isr.Algorithms.Signals.Signal.Frequencies(1.0, fftPoints)

            ' Initialize the last point of the signal to the last point.
            Dim lastSignalPoint As Integer = fftPoints - 1

            ' Initialize the first point of the signal to the last point.
            Dim firstSignalPoint As Integer = 0

            ' Calculate the phase of the last point of the sine wave
            Dim deltaPhase As Single = 2.0F * Convert.ToSingle(Math.PI) *
                Single.Parse(Me._CyclesTextBox.Text, Globalization.CultureInfo.CurrentCulture) / fftPoints

            ' get the signal phase of the last point
            Dim signalPhase As Single = Convert.ToSingle(isr.Algorithms.Signals.Signal.ToRadians(
                                Single.Parse(Me._PhaseTextBox.Text, Globalization.CultureInfo.CurrentCulture)))
            signalPhase += deltaPhase * lastSignalPoint

            ' Use a new frequency for the sine wave to see how
            ' the old is phased out and the new gets in
            deltaPhase *= 2

            ' First element in the previous value of the time series
            Dim oldValue As Complex

            ' New value from the Signal
            Dim newValue As Complex

            ' Create a new instance of the sliding FFT class
            Using slidingFft As isr.Algorithms.Signals.SlidingFourierTransform = New isr.Algorithms.Signals.SlidingFourierTransform

                ' Initialize sliding FFT coefficients.
                slidingFft.Initialize(fftValues)

                ' Recalculate the sliding FFT
                Do While Me._StartStopCheckBox.Checked

                    ' Allow other events to occur
                    My.Application.DoEvents()

                    ' ------- Calculate FFT outcomes ---------

                    ' You must re-map the FFT utilities if using more than one time series at a time
                    ' isr.Algorithms.Signals.Helper.Map(real, imaginary)

                    ' Get the FFT magnitudes
                    magnitudes = isr.Algorithms.Signals.ComplexArrays.Magnitudes(fftValues)

                    ' Get the Frequency
                    frequencies = isr.Algorithms.Signals.Signal.Frequencies(1.0, fftPoints)

                    ' ------------ Plot the Signal ---------------

                    Me.chartSignal(isr.Algorithms.Signals.Signal.Ramp(Single.Parse(Me._SignalDurationTextBox.Text,
                                                                                 Globalization.CultureInfo.CurrentCulture) / fftPoints, fftPoints),
                                                                         signal)

                    ' ------------ Plot FFT outcome ---------------

                    chartAmplitudeSpectrum(frequencies, magnitudes, 1)

                    ' Update the previous values of the signal
                    oldValue = New Complex(signal(firstSignalPoint), 0)

                    ' Add some random noise to make it interesting.
                    Dim noise As Double = noiseFigure * 2.0F * (Microsoft.VisualBasic.Rnd() - 0.5F)

                    ' Get new signal values.
                    signalPhase += deltaPhase
                    newValue = New Complex(System.Math.Sin(signalPhase) + noise, 0)

                    ' Update the last point.
                    lastSignalPoint += 1
                    If lastSignalPoint >= fftPoints Then
                        lastSignalPoint = 0
                    End If

                    ' Update the first point.
                    firstSignalPoint += 1
                    If firstSignalPoint >= fftPoints Then
                        firstSignalPoint = 0
                    End If

                    ' Place new data in the signal itself.
                    signal(lastSignalPoint) = CSng(newValue.Real)

                    ' You must re-map the FFT utilities if using more than one time series at a time
                    ' isr.Algorithms.Signals.Helper.Map(real, imaginary)

                    ' Calculate the sliding FFT coefficients.
                    slidingFft.Update(newValue, oldValue)
                Loop
            End Using
        End Using

    End Sub

#End Region

#Region " CHARTING "

#Region " AMPLITUDE SPECTRUM CHART "

    ''' <summary>Gets or sets reference to the amplitude spectrum curve.</summary>
    Private amplitudeSpectrumCurve As isr.Drawing.Curve

    ''' <summary>Gets or sets reference to the spectrum amplitude axis.</summary>
    Private amplitudeSpectrumAxis As isr.Drawing.Axis

    ''' <summary>Gets or sets reference to the spectrum frequency axis.</summary>
    Private frequencyAxis As isr.Drawing.Axis

    ''' <summary>Charts spectrum.</summary>
    Private _spectrumChartPan As isr.Drawing.ChartPane

    ''' <summary> Creates the chart for displaying the amplitude spectrum. </summary>
    Private Sub createSpectrumChart()

        ' set chart area and titles.
        Me._spectrumChartPan = New isr.Drawing.ChartPane
        Me._spectrumChartPan.PaneArea = New RectangleF(10, 10, 10, 10)
        Me._spectrumChartPan.Title.Caption = "Spectrum"
        Me._spectrumChartPan.Legend.Visible = False

        frequencyAxis = Me._spectrumChartPan.AddAxis("Frequency, Hz", isr.Drawing.AxisType.X)
        frequencyAxis.Max = New isr.Drawing.AutoValueR(100, False)
        frequencyAxis.Min = New isr.Drawing.AutoValueR(1, False)

        frequencyAxis.Grid.Visible = True
        frequencyAxis.Grid.LineColor = System.Drawing.Color.DarkGray

        frequencyAxis.TickLabels.Visible = True
        frequencyAxis.TickLabels.DecimalPlaces = New isr.Drawing.AutoValue(1, True)
        ' frequencyAxis.TickLabels.Appearance.Angle = 60.0F

        amplitudeSpectrumAxis = Me._spectrumChartPan.AddAxis("Amplitude, Volts", isr.Drawing.AxisType.Y)
        amplitudeSpectrumAxis.CoordinateScale = New isr.Drawing.CoordinateScale(isr.Drawing.CoordinateScaleType.Linear)

        amplitudeSpectrumAxis.Title.Visible = True

        amplitudeSpectrumAxis.Max = New isr.Drawing.AutoValueR(1, True)
        amplitudeSpectrumAxis.Min = New isr.Drawing.AutoValueR(0, True)

        amplitudeSpectrumAxis.Grid.Visible = True
        amplitudeSpectrumAxis.Grid.LineColor = System.Drawing.Color.DarkGray

        amplitudeSpectrumAxis.TickLabels.Visible = True
        amplitudeSpectrumAxis.TickLabels.DecimalPlaces = New isr.Drawing.AutoValue(0, True)

        Me._spectrumChartPan.AxisFrame.FillColor = Color.WhiteSmoke

        Me._spectrumChartPan.SetSize(Me._SpectrumChartPanel.ClientRectangle)

        amplitudeSpectrumCurve = Me._spectrumChartPan.AddCurve(Drawing.CurveType.XY, "Amplitude Spectrum", frequencyAxis, amplitudeSpectrumAxis)
        amplitudeSpectrumCurve.Cord.LineColor = Color.Red
        amplitudeSpectrumCurve.Cord.CordType = Drawing.CordType.Linear
        amplitudeSpectrumCurve.Symbol.Visible = False
        amplitudeSpectrumCurve.Cord.LineWidth = 1.0F

        Me._spectrumChartPan.Rescale()

    End Sub

    ''' <summary> Plots the amplitude spectrum using the spectrum amplitudes and frequencies. </summary>
    ''' <param name="frequencies"> Holds the spectrum frequencies. </param>
    ''' <param name="magnitudes">  Holds the spectrum amplitudes. </param>
    ''' <param name="scaleFactor"> Specifies the scale factor by which to scale the spectrum for
    ''' plotting data that was not scaled. </param>
    Private Sub chartAmplitudeSpectrum(ByVal frequencies() As Double, ByVal magnitudes() As Double,
                                       ByVal scaleFactor As Double)

        ' adjust abscissa scale.
        frequencyAxis.Min = New isr.Drawing.AutoValueR(frequencies(frequencies.GetLowerBound(0)), False)
        frequencyAxis.Max = New isr.Drawing.AutoValueR(frequencies(frequencies.GetUpperBound(0)), False)

        ' adjust ordinate scale.
        amplitudeSpectrumAxis.Max = New isr.Drawing.AutoValueR(1, True)
        amplitudeSpectrumAxis.Min = New isr.Drawing.AutoValueR(0, True)

        ' Set initial value for frequency
        '    Dim currentFrequency As Double = Gopher.AmplitudeSpectrumChart.AbscissaMin.Value
        '   Dim deltaFrequency As Double = 1.0# / Gopher.Test.EpochDuration

        ' plot all but last, which was plotted above

        ' scale the magnitudes
        Dim amplitudes(frequencies.Length - 1) As Double
        For i As Integer = frequencies.GetLowerBound(0) To frequencies.GetUpperBound(0)

            amplitudes(i) = scaleFactor * magnitudes(i)
            If (i Mod 50) = 0 Then
                Threading.Thread.Sleep(1)
            End If

        Next i

        amplitudeSpectrumCurve.UpdateData(frequencies, amplitudes)
        Me._spectrumChartPan.Rescale()
        Me._SpectrumChartPanel.Invalidate()

    End Sub

    ''' <summary> Redraws the amplitude spectrum Chart. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Paint event information. </param>
    Private Sub spectrumChartPanel_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles _SpectrumChartPanel.Paint
        If sender IsNot Nothing Then
            Using sb As New SolidBrush(Color.Gray)
                e.Graphics.FillRectangle(sb, Me.ClientRectangle)
                Me._spectrumChartPan.Draw(e.Graphics)
            End Using
        End If
    End Sub

    ''' <summary> Redraws the amplitude spectrum chart when the form is resized. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of the chart panel
    ''' <see cref="System.Windows.Forms.Panel"/> </param>
    ''' <param name="e">      <see cref="System.EventArgs"/> </param>
    Private Sub spectrumChartPanel_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SpectrumChartPanel.Resize
        If Not Me._spectrumChartPan Is Nothing Then
            Me._spectrumChartPan.SetSize(Me._SpectrumChartPanel.ClientRectangle)
        End If
    End Sub

#End Region

#Region " SIGNAL CHART "

    ''' <summary>Gets or sets reference to the instantaneous DP curve.</summary>
    Private signalCurve As isr.Drawing.Curve

    ''' <summary>Gets or sets reference to the instantaneous DP amplitude axis.</summary>
    Private signalAxis As isr.Drawing.Axis

    ''' <summary>Gets or sets reference to the instantaneous DP time axis.</summary>
    Private timeAxis As isr.Drawing.Axis

    ''' <summary>Charts spectrum.</summary>
    Private signalChartPane As isr.Drawing.ChartPane

    ''' <summary> Creates the Scope for display of voltages during the epoch. </summary>
    Private Sub createSignalChart()

        signalChartPane = New isr.Drawing.ChartPane

        ' set chart area and titles.
        signalChartPane.PaneArea = New RectangleF(10, 10, 10, 10)
        signalChartPane.Title.Caption = "Voltage versus Time"
        signalChartPane.Legend.Visible = False

        timeAxis = signalChartPane.AddAxis("Time, Seconds", isr.Drawing.AxisType.X)
        timeAxis.Max = New isr.Drawing.AutoValueR(1, False)
        timeAxis.Min = New isr.Drawing.AutoValueR(0, False)

        timeAxis.Grid.Visible = True
        timeAxis.Grid.LineColor = System.Drawing.Color.DarkGray

        timeAxis.TickLabels.Visible = True
        timeAxis.TickLabels.DecimalPlaces = New isr.Drawing.AutoValue(1, True)
        ' timeAxis.TickLabels.Appearance.Angle = 60.0F

        signalAxis = signalChartPane.AddAxis("Volts", isr.Drawing.AxisType.Y)
        signalAxis.CoordinateScale = New isr.Drawing.CoordinateScale(isr.Drawing.CoordinateScaleType.Linear)

        signalAxis.Title.Visible = True

        signalAxis.Max = New isr.Drawing.AutoValueR(1, True)
        signalAxis.Min = New isr.Drawing.AutoValueR(-1, True)

        signalAxis.Grid.Visible = True
        signalAxis.Grid.LineColor = System.Drawing.Color.DarkGray

        signalAxis.TickLabels.Visible = True
        signalAxis.TickLabels.DecimalPlaces = New isr.Drawing.AutoValue(0, True)

        signalChartPane.AxisFrame.FillColor = Color.WhiteSmoke
        signalChartPane.SetSize(Me._SignalChartPanel.ClientRectangle)

        signalCurve = signalChartPane.AddCurve(Drawing.CurveType.XY, "TimeSeries", timeAxis, signalAxis)
        signalCurve.Cord.LineColor = Color.Red
        signalCurve.Cord.CordType = Drawing.CordType.Linear
        signalCurve.Symbol.Visible = False
        signalCurve.Cord.LineWidth = 1.0F

        signalChartPane.Rescale()

    End Sub

    ''' <summary> Plot the DP signal from the entire epoch data simulating a strip chart effect. </summary>
    ''' <param name="times">      The times. </param>
    ''' <param name="amplitudes"> The amplitudes. </param>
    Private Sub chartSignal(ByVal times() As Single, ByVal amplitudes() As Single)

        ' plot all but last, which was plotted above
        Dim newApms(times.Length - 1) As Double
        Dim newTimes(times.Length - 1) As Double
        For i As Integer = times.GetLowerBound(0) To times.GetUpperBound(0)

            newApms(i) = amplitudes(i)
            newTimes(i) = times(i)
            If (i Mod 50) = 0 Then
                Threading.Thread.Sleep(1)
            End If

        Next i

        chartSignal(newTimes, newApms)

    End Sub

    ''' <summary> Plot the DP signal from the entire epoch data simulating a strip chart effect. </summary>
    ''' <param name="times">      The times. </param>
    ''' <param name="amplitudes"> The amplitudes. </param>
    Private Sub chartSignal(ByVal times() As Double, ByVal amplitudes() As Double)

        timeAxis.Max = New isr.Drawing.AutoValueR(Double.Parse(Me._SignalDurationTextBox.Text, Globalization.CultureInfo.CurrentCulture), False)
        timeAxis.Min = New isr.Drawing.AutoValueR(0, False)

        signalAxis.Max = New isr.Drawing.AutoValueR(1, True)
        signalAxis.Min = New isr.Drawing.AutoValueR(-1, True)

        signalCurve.UpdateData(times, amplitudes)
        Me.signalChartPane.Rescale()

        Me._SignalChartPanel.Invalidate()

    End Sub

    ''' <summary> Redraws the Signal. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Paint event information. </param>
    Private Sub _SignalChartPanel_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles _SignalChartPanel.Paint
        If sender IsNot Nothing Then
            Using sb As New SolidBrush(Color.Gray)
                e.Graphics.FillRectangle(sb, Me.ClientRectangle)
                signalChartPane.Draw(e.Graphics)
            End Using
        End If
    End Sub

    ''' <summary> Redraws the Signal chart when the form is resized. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of the chart panel
    ''' <see cref="System.Windows.Forms.Panel"/> </param>
    ''' <param name="e">      <see cref="System.EventArgs"/> </param>
    Private Sub _SignalChartPanel_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SignalChartPanel.Resize
        If Not signalChartPane Is Nothing Then
            signalChartPane.SetSize(Me._SignalChartPanel.ClientRectangle)
        End If
    End Sub

#End Region

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Event handler. Called by _SignalComboBox for validated events. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _SignalComboBox_Validated(sender As Object, e As System.EventArgs) Handles _SignalComboBox.Validated
        Me.UpdateSignal()
    End Sub

    ''' <summary> Event handler. Called by _cyclesTextBox for validating events. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub _cyclesTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _CyclesTextBox.Validating

        Me._ErrorProvider.SetError(Me._CyclesTextBox, String.Empty)
        If Me._CyclesTextBox.Text.IsNumber() Then
            If Integer.Parse(Me._CyclesTextBox.Text, Globalization.CultureInfo.CurrentCulture) < 1 Then
                e.Cancel = True
                Me._ErrorProvider.SetError(Me._CyclesTextBox, "Must exceed 1")
            Else
                Me.UpdateSignal()
            End If
        Else
            e.Cancel = True
            Me._ErrorProvider.SetError(Me._CyclesTextBox, "Enter numeric value")
        End If

    End Sub

    ''' <summary> Event handler. Called by _PhaseTextBox for validating events. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub _PhaseTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _PhaseTextBox.Validating

        Me._ErrorProvider.SetError(Me._PhaseTextBox, String.Empty)
        If Me._PhaseTextBox.Text.IsNumber() Then
            Me.UpdateSignal()
        Else
            e.Cancel = True
            Me._ErrorProvider.SetError(Me._PhaseTextBox, "Enter numeric value")
        End If

    End Sub

    ''' <summary> Event handler. Called by _pointsTextBox for validating events. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub _pointsTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _PointsTextBox.Validating

        Me._ErrorProvider.SetError(Me._PointsTextBox, String.Empty)
        If Me._PointsTextBox.Text.IsNumber() Then
            If Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture) < 2 Then
                e.Cancel = True
                Me._ErrorProvider.SetError(Me._PointsTextBox, "Must exceed 2")
            Else
                ' Set initial values defining the signal
                If Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture) <
                    Integer.Parse(Me._PointsToDisplayTextBox.Text, Globalization.CultureInfo.CurrentCulture) Then
                    Me._PointsToDisplayTextBox.Text = Me._PointsTextBox.Text
                End If
                Me.UpdateSignal()
            End If
        Else
            e.Cancel = True
            Me._ErrorProvider.SetError(Me._PointsTextBox, "Enter numeric value")
        End If

    End Sub

    ''' <summary> Event handler. Called by _pointsToDisplayTextBox for validating events. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub _pointsToDisplayTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _PointsToDisplayTextBox.Validating

        Me._ErrorProvider.SetError(Me._PointsToDisplayTextBox, String.Empty)
        If Me._PointsToDisplayTextBox.Text.IsNumber() Then
            If Integer.Parse(Me._PointsToDisplayTextBox.Text, Globalization.CultureInfo.CurrentCulture) < 2 Then
                e.Cancel = True
                Me._ErrorProvider.SetError(Me._PointsToDisplayTextBox, "Must exceed 2")
            Else
                ' Set initial values defining the signal
                If Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture) <
                    Integer.Parse(Me._PointsToDisplayTextBox.Text, Globalization.CultureInfo.CurrentCulture) Then
                    Me._PointsToDisplayTextBox.Text = Me._PointsTextBox.Text
                End If
            End If
        Else
            e.Cancel = True
            Me._ErrorProvider.SetError(Me._PointsToDisplayTextBox, "Enter numeric value")
        End If

    End Sub

    ''' <summary> Event handler. Called by _startStopCheckBox for checked changed events. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _startStopCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _StartStopCheckBox.CheckedChanged

        If Me._StartStopCheckBox.Checked Then
            Me._StartStopCheckBox.Text = "&Stop"
        Else
            Me._StartStopCheckBox.Text = "&Start"
        End If

        If Me._StartStopCheckBox.Enabled AndAlso Me._StartStopCheckBox.Checked Then

            Dim algorithm As Example = Me.SelectedExample

            Select Case algorithm

                Case Example.DiscreteFourierTransform, Example.MixedRadixFFT

                    Me._CountToolStripStatusLabel.Text = "0"
                    Do
                        If Me._DoubleRadioButton.Checked Then
                            Me.calculateSpectrumDouble(algorithm)
                        Else
                            Me.calculateSpectrumSingle(algorithm)
                        End If
                        My.Application.DoEvents()
                        Dim count As Integer = Integer.Parse(Me._CountToolStripStatusLabel.Text, Globalization.CultureInfo.CurrentCulture) + 1
                        Me._CountToolStripStatusLabel.Text = count.ToString(Globalization.CultureInfo.CurrentCulture)
                    Loop While Me._StartStopCheckBox.Checked And False

                Case Example.SlidingFFT

                    If Me._DoubleRadioButton.Checked Then
                        Me.slidingFft(0.5R)
                    Else
                        Me.slidingFft(0.5F)
                    End If

                Case Else

            End Select

            If Me._StartStopCheckBox.Checked Then
                Me._StartStopCheckBox.Enabled = False
                Me._StartStopCheckBox.Checked = False
                Me._StartStopCheckBox.Enabled = True
            End If

        Else

            Me._CountToolStripStatusLabel.Text = "0"

        End If

    End Sub

#End Region

End Class