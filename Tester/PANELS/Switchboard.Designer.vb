<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Switchboard

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Switchboard))
        Me._ApplicationsListBoxLabel = New System.Windows.Forms.Label()
        Me._OpenButton = New System.Windows.Forms.Button()
        Me._ExitButton = New System.Windows.Forms.Button()
        Me._ApplicationsListBox = New System.Windows.Forms.ListBox()
        Me._Tooltip = New System.Windows.Forms.ToolTip(Me.components)
        Me._PictureBox = New System.Windows.Forms.PictureBox()
        CType(Me._PictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_ApplicationsListBoxLabel
        '
        Me._ApplicationsListBoxLabel.Location = New System.Drawing.Point(14, 9)
        Me._ApplicationsListBoxLabel.Name = "_ApplicationsListBoxLabel"
        Me._ApplicationsListBoxLabel.Size = New System.Drawing.Size(296, 16)
        Me._ApplicationsListBoxLabel.TabIndex = 15
        Me._ApplicationsListBoxLabel.Text = "Select an item from the list and click Open"
        '
        '_OpenButton
        '
        Me._OpenButton.Location = New System.Drawing.Point(355, 101)
        Me._OpenButton.Name = "_OpenButton"
        Me._OpenButton.Size = New System.Drawing.Size(75, 28)
        Me._OpenButton.TabIndex = 13
        Me._OpenButton.Text = "&Open..."
        '
        '_ExitButton
        '
        Me._ExitButton.Location = New System.Drawing.Point(355, 133)
        Me._ExitButton.Name = "_ExitButton"
        Me._ExitButton.Size = New System.Drawing.Size(75, 28)
        Me._ExitButton.TabIndex = 12
        Me._ExitButton.Text = "E&xit"
        '
        '_ApplicationsListBox
        '
        Me._ApplicationsListBox.ItemHeight = 17
        Me._ApplicationsListBox.Location = New System.Drawing.Point(11, 29)
        Me._ApplicationsListBox.Name = "_ApplicationsListBox"
        Me._ApplicationsListBox.Size = New System.Drawing.Size(328, 106)
        Me._ApplicationsListBox.TabIndex = 14
        Me._Tooltip.SetToolTip(Me._ApplicationsListBox, "Select an application to test")
        '
        '_PictureBox
        '
        Me._PictureBox.Cursor = System.Windows.Forms.Cursors.Hand
        Me._PictureBox.Location = New System.Drawing.Point(355, 9)
        Me._PictureBox.Name = "_PictureBox"
        Me._PictureBox.Size = New System.Drawing.Size(73, 86)
        Me._PictureBox.TabIndex = 11
        Me._PictureBox.TabStop = False
        '
        'Switchboard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.ClientSize = New System.Drawing.Size(440, 166)
        Me.Controls.Add(Me._ApplicationsListBoxLabel)
        Me.Controls.Add(Me._OpenButton)
        Me.Controls.Add(Me._ExitButton)
        Me.Controls.Add(Me._ApplicationsListBox)
        Me.Controls.Add(Me._PictureBox)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Switchboard"
        Me.Text = "Switchboard"
        CType(Me._PictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _ApplicationsListBoxLabel As System.Windows.Forms.Label
    Private WithEvents _OpenButton As System.Windows.Forms.Button
    Private WithEvents _ExitButton As System.Windows.Forms.Button
    Private WithEvents _ApplicationsListBox As System.Windows.Forms.ListBox
    Private WithEvents _PictureBox As System.Windows.Forms.PictureBox
    Private WithEvents _Tooltip As System.Windows.Forms.ToolTip
End Class
