Imports isr.Core
''' <summary>Includes code for Switchboard Form, which serves as a switchboard for this program.</summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class Switchboard
    Inherits isr.Core.Diagnosis.FormBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions

        ' This method is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

    End Sub

#Region " Unused "
#If False Then
  ''' <summary>Cleans up managed components.</summary>
  ''' <remarks>Use this method to reclaim managed resources used by this class.</remarks>
  Private Sub onDisposeManagedResources()

  End Sub

  ''' <summary>Cleans up unmanaged components.</summary>
  ''' <remarks>Use this method to reclaim unmanaged resources used by this class.</remarks>
  Private Sub onDisposeUnmanagedResources()

  End Sub

#End If
#End Region

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>Occurs before the form is closed</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.ComponentModel.CancelEventArgs"/></param>
    ''' <remarks>Use this method to optionally cancel the closing of the form.
    ''' Because the form is not yet closed at this point, this is also the best 
    ''' place to serialize a form's visible properties, such as size and 
    ''' location. Finally, dispose of any form level objects especially those that
    ''' might needs access to the form and thus should not be terminated after the
    ''' form closed.
    ''' </remarks>
    Private Sub form_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        ' disable the timer if any
        ' actionTimer.Enabled = False
        My.Application.DoEvents()

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            ' terminate form-level objects
            ' Me.terminateObjects()
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try

    End Sub

    ''' <summary>Occurs when the form is loaded.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>Use this method for doing any final initialization right before 
    '''   the form is shown.  This is a good place to change the Visible and
    '''   ShowInTaskbar properties to start the form as hidden.  
    '''   Starting a form as hidden is useful for forms that need to be running but that
    '''   should not show themselves right away, such as forms with a notify icon in the
    '''   task bar.</remarks>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' instantiate form objects
            Me.instantiateObjects()

            ' set the form caption
            Me.Text = My.Application.Info.BuildDefaultCaption(": SWITCH BOARD")

            ' set user interface
            '      initializeUserInterface()

            ' center the form
            Me.CenterToScreen()

            ' turn on the loaded flag
            '      loaded = True

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub form_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' set the form caption
            Me.Text = My.Application.Info.BuildDefaultCaption(": SWITCH BOARD")

        Catch

            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

#Region " PRIVATE  and  PROTECTED "

    ''' <summary>Enumerates the action options.</summary>
    Private Enum ActionOption
        <System.ComponentModel.Description("Spectrum")> Spectrum
    End Enum

    ''' <summary>Initializes the class objects.</summary>
    ''' <remarks>Called from the form load method to instantiate 
    '''   module-level objects.</remarks>
    Private Sub instantiateObjects()

        ' populate the action list
        Me.populateActiveList()

    End Sub

    ''' <summary>Populates the list of options in the action combo box.</summary>
    ''' <remarks>It seems that out enumerated list does not work very well with
    '''   this list.</remarks>
    Private Sub populateActiveList()

        ' set the action list
        Me._ApplicationsListBox.Items.Clear()
        Me._ApplicationsListBox.DataSource = isr.Core.Primitives.EnumExtensions.ValueDescriptionPairs(GetType(ActionOption))
        Me._ApplicationsListBox.ValueMember = "Key"
        Me._ApplicationsListBox.DisplayMember = "Value"

    End Sub

    ''' <summary>
    ''' Gets the selected action.
    ''' </summary>
    Private ReadOnly Property selectedAction() As ActionOption
        Get
            Return CType(CType(Me._ApplicationsListBox.SelectedItem, System.Collections.Generic.KeyValuePair(Of [Enum], String)).Key, ActionOption)
        End Get
    End Property

#Region " UNUSED "
#If False Then
  ''' <summary>Initializes the user interface and tool tips.</summary>
  ''' <remarks>Call this method from the form load method to set the user interface.</remarks>
  Private Sub initializeUserInterface()
    '    tipsToolTip.SetToolTip(Me.txtDuration, "Enter count-down duration in seconds")
    '    tipsToolTip.SetToolTip(Me.exitButton, "Click to exit")
  End Sub

  ''' <summary>Terminates and disposes of class-level objects.</summary>
  ''' <remarks>Called from the form Closing method.</remarks>
  Private Sub terminateObjects()
  End Sub

#End If
#End Region

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary>Closes the form and exits the application.</summary>
    Private Sub _ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ExitButton.Click

        ' validate controls manually.
        If Not isr.Core.Controls.ControlContainerExtensions.ValidateControls(Me) Then
            ' indicate that the modal dialog is still running
            Me.DialogResult = DialogResult.None
        Else
            Me.Close()
        End If

    End Sub

    ''' <summary>Open selected items.</summary>
    Private Sub openButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _OpenButton.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Select Case Me.selectedAction
            Case ActionOption.Spectrum
                If SpectrumPanel.Instantiated Then
                    My.Forms.SpectrumPanel.Dispose()
                End If
                My.Forms.SpectrumPanel.ShowDialog()
                My.Forms.SpectrumPanel.Dispose()
        End Select
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

#End Region

End Class